<?php defined('C5_EXECUTE') or die("Access Denied."); 
$count = 1;
$src="";
$note='';
$more = 0;
?>

<?php if (isset($error)) { ?>
	<?php echo $error?><br/><br/>
<?php } ?>

<form action="<?php echo $view->url( $resultTargetURL )?>" method="get" class="ccm-search-block-form mar-0">

	<?php if( strlen($title)>0){ ?><h3><?php echo h($title)?></h3><?php } ?>
	<?php if(strlen($query)==0){ ?>
	<input name="search_paths[]" type="hidden" value="<?php echo htmlentities($baseSearchPath, ENT_COMPAT, APP_CHARSET) ?>" />
	<?php } else if (is_array($_REQUEST['search_paths'])) { 
		foreach($_REQUEST['search_paths'] as $search_path){ ?>
			<input name="search_paths[]" type="hidden" value="<?php echo htmlentities($search_path, ENT_COMPAT, APP_CHARSET) ?>" />
	<?php  }
	} ?>
	
	<input name="query" type="text" value="<?php echo htmlentities($query, ENT_COMPAT, APP_CHARSET)?>" class="text-field-subsc  text-search text-search2" placeholder="Search" />
	<button name="submit" type="submit"  class="btn btn-search btn-search2 ccm-search-block-submit" ><span class="glyphicon glyphicon-search"></span></button>

	
    <?php if($buttonText) { ?>
	<input name="submit" type="submit" value="<?php echo h($buttonText)?>" class="btn btn-default ccm-search-block-submit" />
    <?php } ?>

<?php 
$tt = Loader::helper('text');
if ($do_search) {
	if(count($results)==0){ ?>
		<h4 style="margin-top:32px"><?php echo t('There were no results found. Please try another keyword or phrase.')?></h4>	
	<?php }else{ ?>
		<div id="searchResults">
		<div class="divider-50"></div>
		
		<?php foreach($results as $r) {
			$more = 0; 
			$src="";
			if($count > 7){
				$count = 1;
			}
			$currentPageBody = $this->controller->highlightedExtendedMarkup($r->getPageIndexContent(), $query);
			$note= $r->getCollectionDescription();
				if(strlen($note) > 350){
					$note = substr($note ,0 , strpos($note , ' ' , 300));
					$more = 1;
				}
		?>
				<div class="row search-hdng-col">
					<h2><a href="<?php echo $r->getCollectionLink()?>"><?php echo $r->getCollectionName() ?></a></h2>  
				</div>
				<div class="row testim-item-row"> 
					<div class="col-md-5">
						<div class="testim-item<?php echo $count ?> clearfix">
							<div class="testim-img"></div>
								<?php	$thumbnail = $r->getAttribute('thumbnail');
								if(is_object($thumbnail)){
									$src = $thumbnail->getRelativePath();
								} ?>
								<img width="308" height="320" alt="<?php echo ($r->getCollectionName()) ?>" src="<?php echo ($src) ?>" class="testim-img-only">
						</div>
					</div>		
					<div class="col-md-7 testim-img-container">
						<div class="bq-inner">
							<p><?php echo $note; if($more){?><span class="dot-dot">...</span><?php } ?></p>
						</div>
					</div>
				</div>
		<?php 	$count++;}//foreach search result ?>
		</div>
		
		<?php
		
		
       
   		
	} //results found
} 
?>

</form>