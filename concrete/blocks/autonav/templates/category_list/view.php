<?php defined('C5_EXECUTE') or die("Access Denied.");
$nh = Loader::helper('navigation');
$navItems = $controller->getNavItems();
$c = Page::getCurrentPage();



//*** Step 2 of 2: Output menu HTML ***/

if (count($navItems) > 0) {
    echo '<ul>'; //opens the top-level menu

    foreach ($navItems as $ni) {
		
        echo '<li>'; //opens a nav item
		$page = Page::getByID($ni->cID);
		
			$url = $nh->getLinkToCollection($page);
		
        echo '<a href="' . $url . '" target="' . $ni->target . '" class="' . $ni->classes . '">' . $ni->name . '</a>';

        if ($ni->hasSubmenu) {
            echo '<ul>'; //opens a dropdown sub-menu
        } else {
            echo '</li>'; //closes a nav item
            echo str_repeat('</ul></li>', $ni->subDepth); //closes dropdown sub-menu(s) and their top-level nav item(s)
        }
    }

    echo '</ul>'; //closes the top-level menu
} else if (is_object($c) && $c->isEditMode()) { ?>
    <div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Auto-Nav Block.')?></div>
<?php }