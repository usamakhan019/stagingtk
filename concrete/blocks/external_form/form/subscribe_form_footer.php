<?php
$form = Loader::helper('form');
defined('C5_EXECUTE') or die("Access Denied.");
if (isset($response)) { ?>
	<div class="alert alert-danger"><?php echo $response?></div>
<?php } ?>

<div id="sub_alert_msg" class="alert alert-danger hidden_text">
    <p>Valid email address is required</p>
</div>
<form id="subscription_form_footer" class="subscription_form" method="post" action="<?php echo $view->action('subscription_form_footer')?>">
	<input
		class="form-control text-field-subsc"
		name='username'
		id="fusername"
		value=""
		autocomplete="off"
		placeholder="<?php echo t('Your Name')?>"/>
	<input
		class="form-control text-field-subsc"
		name='emailaddress'
		id="femailaddress"
		value=""
		autocomplete="off"
		placeholder="<?php echo t('Your Email Address')?>"/>
	<button class="btn btn-default-sm btn-subsc subscribe_now">Subscribe now</button>
</form>
