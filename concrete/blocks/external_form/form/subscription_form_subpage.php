<?php
$form = Loader::helper('form');
defined('C5_EXECUTE') or die("Access Denied.");
?>

<form id="subscription_form_subpage" class="subscription_form_subpage" method="post" action="<?php echo $view->action('subscription_form_subpage')?>">
	<div>Your Name</div>
	<input
		type="text"
		class="form-control text-field-subsc"
		name='username'
		id="username_sp"
		value=""
		autocomplete="off"/>
	<div class="divider-15"></div>
	<div>Your Email Address</div>
	<input
		type="text"
		class="form-control text-field-subsc"
		name="emailaddress"
		id="emailaddress_sp"
		value=""
		autocomplete="off"/>
		<div class="divider-15"></div>
	<button class="btn btn-default-sm btn-subsc subscribe_now">Subscribe now</button>
</form>
<div id="sub_alert_msg_sp" class="alert alert-danger hidden_text" style="width:265px;z">
    <p>Valid email address is required</p>
</div>