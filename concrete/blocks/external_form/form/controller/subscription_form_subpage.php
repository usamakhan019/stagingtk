<?php
namespace Concrete\Block\ExternalForm\Form\Controller;
use Concrete\Core\Controller\AbstractController;
use Loader;


class subscriptionFormSubpage extends AbstractController
{
	public $APIKey="u6TbUkPe5wjyg/sX7ZMyCpaZEsDpA0Wu5i/eSYB5bj6qtZRINyPUyxYE5/0T3PyQwCEXM625o2/stVDj16HZ17m6NDgoWfK/fFxsRexFKNnajr+pxcej5qy0UtX1AIVVpfi2i7ELBjelMdwR41Ko9Q==";
	public $password="";
	public $ListId = "8bbf84042bc9b8e285827ce697589261";
	public $URL="https://api.createsend.com/api/v3.2/subscribers/";

	function __construct() {
		$this->URL = "{$this->URL}{$this->ListId}.json";
  }

  public function action_subscription_form_subpage($bID = false)
  {
    $ErrorArr = array();
    $emailId = $_POST['email'];
    $username = $_POST['name'];

    if (!(empty($emailId) || empty($username))) {
      if (filter_var($emailId, FILTER_VALIDATE_EMAIL)){
        //get subscriber record
        $record = json_decode($this->getSubscriberRecord($emailId),true);

        if (array_key_exists('Code', $record)){
          
          //Record doesn't exist > New Subscription
          $new_subscriber_info = $this->addSubscriberRecordToActiveList($emailId , $username);
          if (!(is_string($new_subscriber_info) && trim($new_subscriber_info,'"') == $emailId))
            $ErrorArr['error'] = $new_subscriber_info; //"We are unable to add a new subscriber. Please contact The Hampstead Kitchen for further assistance.";
        
        }else{
          switch (strtolower($record['State'])){
            case "active":
              $ErrorArr['error'] = "You are already subscribed to our newsletter campaign.";
              break;
            case "unsubscribed":
              $unsubscribed_user_info = $this->addSubscriberRecordToActiveList($emailId, $username);
              if (!(is_string($unsubscribed_user_info) && trim($unsubscribed_user_info,'"') == $emailId))
                $ErrorArr['error'] = "Thank you for subscribing to our newsletter campaign.";
              break;
            case "deleted":
              $ErrorArr['error'] = "Your subscription had been removed. Please contact The Hampstead Kitchen for further assistance.";
              break;
            default :
              #Test bounced status
              $ErrorArr['error'] = "Our newsletter emails to you on the entered address are getting bounced. Please try subscribing with a valid and active email address.";
          }
        }

      }else{
        $ErrorArr['error'] = "Please enter a valid  email address";
      }

    }else{
      $ErrorArr['error'] = "Please fill in your username and email address";
    }
    echo json_encode($ErrorArr);
    exit;

  }
  
  public function getSubscriberRecord($emailId){
  	$url = $this->URL;
  	$url = $url."?email={$emailId}";
  	$auth = "$this->APIKey:$this->password";
  	$ch = curl_init();
  	$options = array(
  		CURLOPT_URL => $url,
  		CURLOPT_TIMEOUT => 30,
  		CURLOPT_RETURNTRANSFER => 1,
  		CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
  		CURLOPT_HTTPHEADER => array('Content-Type: application/json'),
  		CURLOPT_USERPWD => $auth,
  		CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13'
  	);
  	curl_setopt_array($ch, $options);
  	$result = curl_exec ($ch);
  	curl_close($ch);
  	return $result;
  }

  public function addSubscriberRecordToActiveList($emailId, $username){
  	$subscriptionTime  =  date("Y/m/d G:i:s");
		$subscriberInfo = array(
    	"EmailAddress" =>  $emailId ,
      "name" =>  $username ,
    	"Resubscribe" => true,
      "RestartSubscriptionBasedAutoresponders" => true,
      "ConsentToTrack" => "Yes"
  	);

  	$url = $this->URL;
  	$auth = "$this->APIKey:$this->password";
  	$ch = curl_init();
  	$options = array(
  		CURLOPT_URL => $url,
  		CURLOPT_TIMEOUT => 30,
  		CURLOPT_RETURNTRANSFER => 1,
  		CURLOPT_POST => 1,
  		CURLOPT_POSTFIELDS => json_encode($subscriberInfo),
  		CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
  		CURLOPT_HTTPHEADER => array('Content-Type: application/json'),
  		CURLOPT_USERPWD => $auth,
  		CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13'
  	);
  	curl_setopt_array($ch, $options);
  	$result = curl_exec ($ch);
  	curl_close($ch);
  	return $result;

  }
}

