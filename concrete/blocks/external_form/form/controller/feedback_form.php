<?php
namespace Concrete\Block\ExternalForm\Form\Controller;
use Concrete\Core\Form\Service;
use Loader;
use Concrete\Core\Controller\AbstractController;

class FeedbackForm extends AbstractController
{

    public function action_test_search($bID = false)
    {
		    $invalid_inputs = array();

		    $mh = Loader::helper('mail');
            $q1_occasion = $_POST['occasion'];
            $q2_howmany = $_POST['howmany'];
            $q3_date = $_POST['date_q'];
            $q4_name = $_POST['name'];
            $q5_email = $_POST['email'];
            $q6_message = $_POST['message'];


       if($q5_email == "Your Email or Phone No" || $q5_email == null || $q5_email == ""){
          $invalid_inputs['Question5'] = 5;
       }
 

       if(count($invalid_inputs) == 0)
       {  
		  $pos = strpos($q5_email , '@');
		  if($pos != false){
			$body_message = <<<EOD
                  <p>There has been a submission of the form Just a few things to get started.</p>
                  <br><strong>What's the occasion?</strong><br>
                  {$q1_occasion}
                  <br><br><strong>For how many?</strong><br>
                  {$q2_howmany}
                  <br><br><strong>Date?</strong><br>
                  {$q3_date}
                  <br><br><strong>Name & Location</strong><br>
                  {$q4_name}
                  <br><br><strong>Your Email or Phone No</strong>
                  <br><a href="mailto:{$q5_email}">{$q5_email}</a>
                  <br><br><strong>Your Requirements</strong>
                  <br>{$q6_message}
                  <p>Many Thanks</p>
                  <a href="mailto:feedme@thehampsteadkitchen.com">feedme@thehampsteadkitchen.com</a>
EOD;
		  }else{
			 $body_message = <<<EOD
                  <p>There has been a submission of the form Just a few things to get started.</p>
                  <br><strong>What's the occasion?</strong><br>
                  {$q1_occasion}
                  <br><br><strong>For how many?</strong><br>
                  {$q2_howmany}
                  <br><br><strong>Date?</strong><br>
                  {$q3_date}
                  <br><br><strong>Name & Location</strong><br>
                  {$q4_name}
                  <br><br><strong>Your Email or Phone No</strong>
                  <br><a href="tel:{$q5_email}">{$q5_email}</a>
                  <br><br><strong>Your Requirements</strong>
                  <br>{$q6_message}
                  <p>Many Thanks</p>
                  <a href="mailto:feedme@thehampsteadkitchen.com">feedme@thehampsteadkitchen.com</a>
EOD;
		  }
         
            $mh->setSubject('Just a few things to get us started. Form Submission');
            $mh->setBodyHTML($body_message);
            $mh->to("feedme@thehampsteadkitchen.com");
            $mh->sendMail(); 
       }
      echo json_encode($invalid_inputs);
      
		exit;
    
    }

    public function view()
    {
        $this->set('message', t('This is just an example of how a custom form works.'));
    }
}
