<?php
$form = Loader::helper('form');
defined('C5_EXECUTE') or die("Access Denied.");
 ?>
	


<div class="" style="display : none;" id="MessageBox">
   <p></p>
</div>
<form id="miniSurveyView" class="miniSurveyView" method="post" action="<?php echo $view->action('test_search')?>" >
    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input class="inputNo input1" name="Question1" id="Question1" value="Whats the occasion?"  onfocus="if (this.value=='Whats the occasion?') this.value='';" onblur="this.value = this.value==''?'Whats the occasion?':this.value;" type="text">
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
			<input class="inputNo input2" name="Question2" id="Question2" value="For how many?" onfocus="if (this.value=='For how many?') this.value='';" onblur="this.value = this.value==''?'For how many?':this.value;" type="text">
		</div>
	</div>
    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
			<input class="inputNo input3" name="Question3" id="Question3" value="Date?" onfocus="if (this.value=='Date?') this.value='';" onblur="this.value = this.value==''?'Date?':this.value;" type="text">
        </div>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input class="inputNo input4" name="Question4" id="Question4" value="Name & Location" onfocus="if (this.value=='Name & Location') this.value='';" onblur="this.value = this.value==''?'Name & Location':this.value;" type="text">
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<input class="inputNo input5" name="Question5" id="Question5" value="Your Email or Phone No" onfocus="if (this.value=='Your Email or Phone No') this.value='';" onblur="this.value = this.value==''?'Your Email or Phone No':this.value;" >
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<textarea name="Question6" id="Question6" class="input6" cols="50" rows="3" onfocus="if (this.value=='Your Requirements') this.value='';" onblur="this.value = this.value==''?'Your Requirements':this.value;">Your Requirements</textarea>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<input class="submit" name="Submit"  value="Send Now" type="submit"> 
			<div class="close-btn-new-style" id="hideForm">Cancel</div>
		</div>
	</div>
</form>
<script type="text/javascript">
$(document).ready(function() {
	

var url = $("#miniSurveyView").attr('action');



	$("#miniSurveyView").submit(function(){
		ga('send', 'event', 'Form', 'Submit', 'Feedback-Form');
		$("form.miniSurveyView :input").removeClass('lt-error');
		$("form.miniSurveyView .input6").removeClass('lt-error');
		var occasion_quest = $("#Question1").val(),
		how_many_quest = $("#Question2").val(),
		date_quest = $("#Question3").val(),
		name_quest = $("#Question4").val(),
		email_quest = $("#Question5").val(),
		message_quest = $("#Question6").val();

		//bypass sent email condition.
		$(".letsTalk-form").hide('slow', function() {
			$(".success_Message").show('slow');
			$("form.miniSurveyView :input").removeClass('lt-error');
			$("form.miniSurveyView .input6").removeClass('lt-error');
		});


		//console.log (how_many_quest);
		//console.log(url);
		var msg = $("#MessageBox");
		$.ajax({
			url: url,
			type  : "POST",
			data:{occasion : occasion_quest, howmany : how_many_quest, date_q : date_quest, name : name_quest, email : email_quest, message : message_quest},
			success: function(result){
				console.log(result);
				var arr = $.parseJSON(result);
				console.log(arr);
				if(arr instanceof Array)
				{
					$("#miniSurveyView")[0].reset();
					 console.log("success");
				}else{
					$.each(arr, function(key) {
    					key_val = "#" + key;
    					$(key_val).addClass('lt-error');
					});
				}
			
			}
		}); 
	   return false;
	});
});


</script>

