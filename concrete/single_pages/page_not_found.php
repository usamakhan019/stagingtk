<?php defined('C5_EXECUTE') or die("Access Denied.");
$view->inc('elements/header.php');
?>
<div  class="blog-page">
	<div class="container"> 
        	<div class="row">
            		<div class="col-md-offset-3 col-md-6 text-center">
				<div class="pnf-subscription-popup">
        				<div class="text-center">
						<?php 
							$areapagenfound = new Area("page not found message");
							$areapagenfound ->display($c);
						?>  
        				</div>
				</div>
	    		</div>
			<div class="divider-15"></div>
		</div>
	</div>
</div>
<?php $view->inc('elements/footer_ourFood.php'); ?>
