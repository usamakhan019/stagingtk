<?php
defined('C5_EXECUTE') or die("Access Denied.");
$form = Core::make('helper/form');
$val = Core::make('token');
$u = new User();
?>

<?php if ($displayForm && $displayPostingForm == $position) { ?>

	

	<?php if ($enablePosting == Conversation::POSTING_ENABLED) { ?>
	<div class="post-a-comment">
		<a href="#postComment" class="collapsed arrow-position" data-toggle="collapse" aria-expanded="false" aria-controls="postComment">
					<h2>Post a Comment <span class="glyphicon caret"></span></h2>
		</a>
		<div class="collapse" id="postComment">	
			<div class="ccm-conversation-add-new-message" rel="main-reply-form">
				<form method="post" class="main-reply-form">
					<div class="ccm-conversation-errors alert alert-danger"></div>
					<?php Loader::element('conversation/message/author');?>
					
					<div class="ccm-conversation-message-form">
						<div class="row">
							<div class="col-md-3">
								<label class="control-label" for="cnvMessageAuthor_Message"><?php echo t('Your Message *')?></label>
							</div>
							<div class="col-md-9">
								<?php $editor->outputConversationEditorAddMessageForm(); ?>
							</div>
						</div> <!-- End Row -->
						<div class="divider-15"></div>
						
						<?php echo $form->hidden('blockAreaHandle', $blockAreaHandle) ?>
						<?php echo $form->hidden('cID', $cID) ?>
						<?php echo $form->hidden('bID', $bID) ?>
						<div class="row">
                    		<div class="col-md-3"> </div>
                            <div class="col-md-9">
								<button type="button" data-post-parent-id="0" data-submit="conversation-message" class="btn btn-submit btn-postComment"><?php echo t('Post a Comment')?></button>
							</div>
						</div> <!-- End Row -->
						
						
						<?php if ($conversation->getConversationSubscriptionEnabled() && $u->isRegistered()) { ?>
							<a href="<?php echo URL::to('/ccm/system/dialogs/conversation/subscribe', $conversation->getConversationID())?>" data-conversation-subscribe="unsubscribe" <?php if (!$conversation->isUserSubscribed($u)) { ?>style="display: none"<?php } ?> class="btn pull-right btn-default"><?php echo t('Un-Subscribe')?></a>
							<a href="<?php echo URL::to('/ccm/system/dialogs/conversation/subscribe', $conversation->getConversationID())?>" data-conversation-subscribe="subscribe" <?php if ($conversation->isUserSubscribed($u)) { ?>style="display: none"<?php } ?> class="btn pull-right btn-default"><?php echo t('Subscribe to Conversation')?></a>
						<?php } ?>
					</div>
				</form>		
			</div>
		</div>
	</div>
	<?php } else { ?>
		<?php switch($enablePosting) {
			case Conversation::POSTING_DISABLED_MANUALLY:
				print '<p>' . t('Adding new posts is disabled for this conversation.') . '</p>';
				break;
			case Conversation::POSTING_DISABLED_PERMISSIONS:
				print '<p>';
				print ' ';
				if (!$u->isRegistered()) {
					print t('You must <a href="%s">sign in</a> to post to this conversation.', URL::to('/login'));
				} else {
					print t('You do not have permission to post this to conversation.');
				}
				print '</p>';
				break;
		} ?>
	<?php } ?>

<?php } ?>