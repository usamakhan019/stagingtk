<?php
defined('C5_EXECUTE') or die("Access Denied.");
$u = new User();
?>
<?php if ($u->isRegistered()) { ?>
		
<?php } else {
	// non-logged-in posting. ?>
	<div class="divider-15"></div>
	<div class="row">
        <div class="col-md-3">
				<label class="control-label" for="cnvMessageAuthorName"><?php echo t('Your Name *')?></label>
		</div>
        <div class="col-md-9">
			<input type="text" class="form-control input-field" name="cnvMessageAuthorName" />
		</div>
    </div> <!-- End Row -->
    <div class="divider-15"></div>
	
	<div class="row">
        <div class="col-md-3">
				<label class="control-label" for="cnvMessageAuthorEmail"><?php echo t('Your Email *')?></label>
		</div>
        <div class="col-md-9">
			<input type="text" class="form-control input-field" name="cnvMessageAuthorEmail" />
		</div>
    </div> <!-- End Row -->
    <div class="divider-15"></div>
<?php } ?>
