<?php
defined('C5_EXECUTE') or die("Access Denied.");
?>

<div class="ccm-conversation-message-count ff-arial"><h2><?php echo t2('%d Comment', '%d Comments', $conversation->getConversationMessagesTotal())?></h2></div>