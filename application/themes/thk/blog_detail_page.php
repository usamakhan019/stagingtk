<?php
defined('C5_EXECUTE') or die("Access Denied.");
$th = Loader::helper('text');
$nh = Loader::helper('navigation');
$c = Page::getCurrentPage();
$BlogPostURL = urlencode($c->getCollectionLink(true));
$BlogPostDescription = $c->getCollectionDescription();
$category_Page = Page::getByID($c->getCollectionParentID()); 
$category_name = ($category_Page->getCollectionName());
$purl = $nh->getLinkToCollection($category_Page);

$this->inc('elements/header.php');?>
		
<div id="instafeed" style="display : none;"></div>

<div class="blogdetail-page">
	<div class="container"> 
		<div class="row">
			<div class="col-md-8">
				<h1 class="post-cat-title"><?php echo t($category_name) ?></h1>
				<p class="blog-detail-desc"><?php echo $BlogPostDescription; ?></p>
				<div class="post-detail">
					<h2><strong><?php echo ($th->entities($c->getCollectionName()));?></strong></h2>
					<?php 
						$area_content = new Area("Content");
						$area_content->display($c);
					?> 
					<p><a  href="<?php echo ($purl) ?> ">Read more on <?php echo ($category_name); ?></a></p>
					<p class="post-social clearfix">
						<span class="pull-left">Share on:</span> 
						<a href="http://www.facebook.com/sharer.php?u=<?php echo $BlogPostURL; ?>" title="Share at Faceboook" class="icon-fb pull-left share-popup"></a> 
						<a href="http://twitter.com/intent/tweet?url=<?php echo $BlogPostURL; ?>" title="Post on Twitter" class="icon-tw pull-left share-popup"></a> 
						<a href="https://plus.google.com/share?url=<?php echo $BlogPostURL; ?>" title="Share at Google +" class="icon-gp pull-left share-popup"></a>
					</p>
					<div class="clearfix text-center">
						<img src="/application/themes/thk/images/sh-seprator_black.png" alt="section seperator" class="img-responsive center-block" />
					</div>
					<?php 
						$area_comment_form = new Area("conmment_form");
						$area_comment_form->display($c);
					?>  
				</div>	 <!-- post detail end -->
				<div class="fly paint1 mobile-hide hidden"></div>
				<div class="fly scrib-parsley1 mobile-hide"></div>
				<div class="fly paint1 mobile-hide"></div>
			</div> <!-- col 8 -->
			
			<div class="col-md-4 blog-right-col mobile-hide">
				<div class="divider-25"></div>
				<div class="blog-subscibe col-sm-6 col-md-12">
					<?php 
						$area_sub_form = new Area("Subscription Form");
						$area_sub_form->display($c);
					?>  
					<div class="divider-50"></div>
				</div>
				<!-- Blog Subscribe -->
				
				
				<div class="blog-cat-list col-sm-6 col-md-12">
					<?php 
						$area_cat_list = new Area("Category List");
						$area_cat_list->display($c);
					?>  
				</div>
				<div class="divider-50"></div>
				
				<div class="blog-morePosts">
					<?php 
						$area_moreposts = new Area("More_posts");
						$area_moreposts->display($c);
					?> 
				</div>
					<div class="fly scrib-rose3 mobile-hide"></div>
					<div class="vdsec-divider"></div>
            </div><!-- Col 4 -->
		</div><!-- /.row -->
		
		 <div class="row instagram-feed">
    	<div class="col-sm-4 text-center mobile-hide hidden-sm"><h2><img src="/application/themes/thk/images/h3_top_x2.png" alt="Instagram sideline" width="100%" height="18"></h2></div>
        <div class="col-sm-12 col-md-4 text-center">
			<?php 
					$area_video = new Area("Instagram feed header - Blog detail page");
					$area_video->display($c);
			?>
			
		</div>
        <div class="col-sm-4 text-center mobile-hide hidden-sm"><h2><img src="/application/themes/thk/images/h3_top_x2.png" alt="Instagram sideline" width="100%" height="18"></h2></div>
		<div class="divider-30"></div>
		<div class="col-md-12">
			<div id="carousel-instagramfeed" class="carousel slide" data-ride="carousel">                
                  <div class="carousel-inner" role="listbox" id="carousel_container">
					 
				  </div> <!-- Inner Carousel -->
				  <!-- Controls -->
                  <a class="left carousel-control" href="#carousel-instagramfeed" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#carousel-instagramfeed" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
			</div> <!-- carousel End -->
		</div>
    </div><!-- Instagram /.row -->
		
		<div class="block-end-msg col-md-12 text-center">    
			<div class="clearfix text-center">
				<img class="img-responsive center-block" alt="section seperator" src="/application/themes/thk/images/sh-seprator_white.png">
			</div>  
		</div>
    </div><!-- Container -->




<?php  $this->inc('elements/footer.php'); ?>
</div><!-- Page -->