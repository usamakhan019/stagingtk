<?php
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php');  ?>

<div id="instafeed" style="display : none;"></div>

<div  class="blog-page search-page">
	<div class="container"> 	
		<div class="row">
			<div class="col-md-8 blog-page-top">
				<div class="col-md-7">
					<h1 class="mar-t0">
						<?php 
							$area_blog_header = new Area("Search Page Title");
							$area_blog_header->display($c);
						?>
					</h1>
				</div>	
				
				<div class="col-md-12">
					<div class="divider-50"></div>
				</div>
				
				<div class="row">
					<!-- put search results here -->
					<div class="col-md-12 col-sm-12 search-result blogpage-search">
					<?php 
							$area_search_results = new Area("Search results");
							$area_search_results->display($c);
					?>
					</div>
				</div>
					
			</div><!-- Col Left -->
			<div class="col-md-4 blog-right-col">
				<div class="blog-subscibe col-sm-6 col-md-12">
						<?php 
								$area_log_subscribe= new Area("Blog subscription_2");
								$area_log_subscribe->display($c);
						?>
					<div class="divider-50"></div>
				</div>
				<!-- Blog Subscribe -->
				
				<div class="blog-cat-list col-sm-6 col-md-12">
					<?php 
							$area_categoriesListing = new Area("categories List_2");
							$area_categoriesListing->display($c);
					?>
				</div>

				<div class="divider-50"></div>
				<div class="blog-morePosts">	
					<?php 
						$area_blog_posts = new Area("More Blog Posts_2");
						$area_blog_posts->display($c);
					?>
				</div>
			</div><!-- Col 4 --><!-- Col Right -->
		</div><!-- /.row -->
	</div><!-- /.container -->
	
	<div class="container"> 
		 <div class="row">
         <div class="col-md-12">
            	<div class="divider-50"></div>
				<?php 
					$area_blog_posts_slider = new Area("Blog posts slider_2");
					$area_blog_posts_slider->display($c);
				?>
        </div><!-- Col 12 -->

    </div><!-- /.row -->
        
        
        

    <div class="row">
        <div class="divider-50"></div>
        <div class="col-md-3 col-md-offset-1">
			<?php 
				$area_video = new Area("Video_area_2");
				$area_video->display($c);
			?>
        </div>
        <div class="col-md-8">
        	<div class="blog-video-contianer">
                
				<?php 
					$area_video = new Area("Video_frame_2");
					$area_video->display($c);
				?>
               
            </div>
        </div>
    </div><!-- /.row -->
	
	 
    <div class="row instagram-feed">
    	<div class="col-sm-4 text-center mobile-hide hidden-sm"><h2><img src="/application/themes/thk/images/h3_top_x2.png" alt="Instagram feed sideline" width="100%" height="18"></h2></div>
        <div class="col-sm-12 col-md-4 text-center">
			<?php 
					$area_video = new Area("Instagram feed header_2");
					$area_video->display($c);
			?>
			
		</div>
        <div class="col-sm-4 text-center mobile-hide hidden-sm"><h2><img src="/application/themes/thk/images/h3_top_x2.png" alt="Instagram feed sideline" width="100%" height="18"></h2></div>
		<div class="divider-30"></div>
		<div class="col-md-12">
			<div id="carousel-instagramfeed" class="carousel slide" data-ride="carousel">                
                  <div class="carousel-inner" role="listbox" id="carousel_container">
					 
				  </div> <!-- Inner Carousel -->
				  <!-- Controls -->
                  <a class="left carousel-control" href="#carousel-instagramfeed" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#carousel-instagramfeed" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
			</div> <!-- carousel End -->
		</div>
    </div><!-- Instagram /.row -->

	
    
<div class="block-end-msg col-md-12 text-center">    
        <div class="clearfix text-center">
            <img class="img-responsive center-block" alt="line seperator" src="/application/themes/thk/images/sh-seprator_white.png">
        </div>  
    </div>
</div><!-- /.container -->
	
<?php  $this->inc('elements/footer.php'); ?>
</div>