<?php
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php'); ?>
<div class="whatwedo-page">
	<div class="container">
	
	    <!-- First Section, Intro and polaroids -->
	    <a name="GivingBack" class="just-for-link"></a>
		<div class="row wwd-block-1 mar-t-75">
			<div class="col-md-6">
				<div class="text-block">
						<?php
								$area_intro = new Area("project_intro");
								$area_intro->display($c);
						?>
				</div>
				<div class="for-first-section"></div>
			</div>
			
			<div class="col-md-6">
				<div class="minheight-490 text-center">
					<div class="picture-frame zindex10 rotate-m15deg top-150">
						<div class="frame-bg"></div>
						<?php
								$area_image_holder_wwd3 = new Area("image holder 3");
								$area_image_holder_wwd3 ->display($c);
						?>
					</div>
					<div class="picture-frame zindex-11 rotate-15deg left-m15 top-80">
						<div class="frame-bg"></div>
						<?php
									$area_image_holder_wwd4 = new Area("image holder 4");
									$area_image_holder_wwd4 ->display($c);
						?>
					</div>
				</div>
			</div>
			<div class="for-right-side"></div>
		</div> <!-- End first section intro -->

		
		<!-- Second section -->
		<a name="HomeLessVulnerable" class="just-for-link"></a>
		<div class="row wwd-block-2 mt-60">
			<div class="col-md-6 hidden-xs hidden-sm">
				<div class="text-center picture-block minheight-490">
					<div class="picture-frame rotate-m20deg eventwc-pos-2 mr-l-20">
						<div class="frame-bg"></div>
						<?php
							$area_image_holder_wwd2 = new Area("image holder 2");
							$area_image_holder_wwd2 ->display($c);
						?>
					</div>
					<div class="for-second-section mobile-hide"></div>		
				</div>
			</div>


			<div class="col-md-6">
				<div class="text-block">
				  <?php
						$area_block11 = new Area('feeding_homeless');
						$area_block11->display($c);
					?>
				</div>
			</div> 

			<div class="col-md-6 hidden-lg hidden-md">
				<div class="text-center picture-block minheight-490 just-for-mobile">
					<div class="picture-frame rotate-m20deg eventwc-pos-2 mr-l-20">
						<div class="frame-bg"></div>
						<?php
							$area_image_holder_wwd2 = new Area("image holder 2");
							$area_image_holder_wwd2 ->display($c);
						?>
					</div>
					<div class="for-second-section"></div>
				</div>
			</div>       
		</div> <!-- End second section, Events we cater -->	

	
	
		<!-- blank space -->
		<a name="LocalCommunity" class="just-for-link"></a>
		<div class="row wwd-block-3">
	    	<div class="col-md-6 campaigns">
	    		<div class="text-block">
						<?php
								$area_campaign = new Area("campaigns");
								$area_campaign->display($c);
						?>
					</div>
	      </div>
	      <div class="col-md-6 campaign-images">
	      	<div class="picture-block minheight-490 text-center">
						<div class="picture-frame zindex10 rotate-m15deg top-175">
							<div class="frame-bg"></div>
							<?php
									$area_image_holder_wwd3 = new Area("image holder 5");
									$area_image_holder_wwd3 ->display($c);
							?>
						</div>
						<div class="picture-frame zindex-11 rotate-15deg left-m15 top-100">
							<div class="frame-bg"></div>
							<?php
										$area_image_holder_wwd4 = new Area("image holder 6");
										$area_image_holder_wwd4 ->display($c);
							?>
						</div>
					</div>
					<div class="cheery-for-third-section"></div>
	      </div>
	    </div>

	    <a name="RefugeeCampaigns" class="just-for-link"></a>
		<div class="row wwd-block-1">
			<div class="col-md-6 campaign-images hidden-sm hidden-xs">
				<div class="picture-block minheight-490 text-center">
					<div class="picture-frame zindex10 rotate-m15deg top-240">
						<div class="frame-bg"></div>
						<?php
								$area_image_holder_wwd3 = new Area("image holder 7");
								$area_image_holder_wwd3 ->display($c);
						?>
					</div>
					<div class="picture-frame zindex-11 rotate-15deg left-m15 top-160">
						<div class="frame-bg"></div>
						<?php
									$area_image_holder_wwd4 = new Area("image holder 8");
									$area_image_holder_wwd4 ->display($c);
						?>
					</div>
					<div class="for-fourth-section mobile-hide"></div>
				</div>
			</div>

			<div class="col-md-6 campaigns">
				<div class="text-block">
						<?php
								$area_tragedy = new Area("tragedy");
								$area_tragedy->display($c);
						?>
				</div>
			</div>

			<div class="col-md-6 campaign-images hidden-lg hidden-md">
				<div class="picture-block minheight-490 text-center">
					<div class="picture-frame zindex10 rotate-m15deg top-240">
						<div class="frame-bg"></div>
						<?php
								$area_image_holder_wwd3 = new Area("image holder 7");
								$area_image_holder_wwd3 ->display($c);
						?>
					</div>
					<div class="picture-frame zindex-11 rotate-15deg left-m15 top-160">
						<div class="frame-bg"></div>
						<?php
									$area_image_holder_wwd4 = new Area("image holder 8");
									$area_image_holder_wwd4 ->display($c);
						?>
					</div>
					<div class="for-fourth-section"></div>
				</div>
			</div>
		</div> 
	
	<!-- Cuisine selection, and Food Presentation -->
		
		<!-- Row 3 -->
		<a name="InternationalCharities" class="just-for-link"></a>
		<div class="row wwd-block-5">
			<div class="col-md-6">
				<div class="mt-160">
					<?php
						$area_int = new Area("international_charities");
						$area_int->display($c);
					?>
				</div>
			</div>
			<div class="col-md-6">
				<div class="divider-15"></div>
				<div class="picture-block text-center minheight-490">
        	<div class="picture-frame rotate-m15deg zindex10 right-m15 top-260">
            <div class="frame-bg"></div>
            <?php
							$area_image_holder_9 = new Area("image holder 11");
							$area_image_holder_9 ->display($c);
						?>
          </div>
          <div class="picture-frame rotate-10deg zindex10 last-section">
            <div class="frame-bg"></div>
            <?php
							$area_image_holder_10 = new Area("image holder 12");
							$area_image_holder_10 ->display($c);
						?>
          </div>
  			</div>
			</div>
			<div class="for-fifth-section"></div>
		</div><!-- Block 5 -->
		<!-- Row 4 -->

		<!-- community & chairty -->
		<a name="CharitySupport" class="just-for-link"></a>
		<section class="community_charity_logos-section">
			<div class="container">
		        <div class="row">
		          <div class="col-md-12 text-center">
						<?php
							$community_charity_logos = new Area('community_charity_logos');
							$community_charity_logos->display($c);
						?>
		          </div>
		        </div>
			</div>
		</section>
		<!-- end -->
	

		<div class="row wwa-block-8">
			<div class="block-end-msg col-md-12 text-center">
				<?php
						$area_block13 = new Area('what_we_do');
						$area_block13->display($c);
				?>
				<div class="for-last-section mobile-hide"></div>
				<div class="clearfix text-center">
					<img src="/application/themes/thk/images/sh-seprator_white.png" alt="section seperator" class="img-responsive center-block" />
				</div>
			</div>
		</div><!-- Block 8 -->


		<div class="fly scrib1 mobile-hide hidden"></div>
		<div class="fly yellow-sticky hidden"></div>
		<div class="fly paint1 mobile-hide hidden"></div>
		<div class="fly paint3 mobile-hide hidden"></div>


	</div><!-- /.container -->
		<?php  $this->inc('elements/footer.php'); ?>
</div>