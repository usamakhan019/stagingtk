<?php
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php'); ?>

<div class="ourfood-page">
<div class="container"> 
    <div class="row testim-tsec mar-t-75">
        <div class="col-sm-12 col-md-12 col-lg-9">
			<?php 
				$area_ourfood_intro = new Area("Our Food Intro");
				$area_ourfood_intro->display($c);
			?>
        </div>
		 <div class="fly scrib-lavender mobile-hide"></div>
    </div>
</div>

<div class="container">

	<?php 
		$area_Food_sections = new Area("Our_food_sections");
		$area_Food_sections->display($c);
	?>

    <div class="row of-block-6">
			<div class="block-end-msg col-md-12 text-center">
				<?php
					$area_food_footer = new Area("Food_pg_Footer");
					$area_food_footer->display($c);
				?>
                <div class="clearfix text-center">
              		<img src="/application/themes/thk/images/sh-seprator_white.png" alt="line seperator" class="img-responsive center-block" />
              </div>  
            </div>
		<div class="fly doscrib4 doscrib4-2 mobile-hide"></div>    
        <div class="fly scrib-pistasho mobile-hide"></div>
        <div class="fly scrib-pomegranate-wd mobile-hide"></div>
    </div><!-- Block 6 -->
    
</div><!-- /.container -->



<?php  $this->inc('elements/footer.php'); ?>
</div>
