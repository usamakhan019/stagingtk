<?php
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php'); ?>

<div class="ourfooddetail-page">
	<div class="container"> 
		<?php 
			$area_food_slider = new Area("Our_food_slider");
			$area_food_slider->display($c);
		?>
	</div>

<?php  $this->inc('elements/footer_ourFood.php'); ?>
</div>