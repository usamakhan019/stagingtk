<?php
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php'); ?>
<div class="whatwedo-page">
	<div class="container">
	
	    <!-- First Section, Intro and polaroids -->
		<a name="WeStartWithYou" class="just-for-link"></a>
		<div class="row wwd-block-1 mar-t-75">
			<div class="col-md-6">
				<div class="text-block">
					<?php
						$area_events_whatwedo = new Area("what_we_do_intro");
						$area_events_whatwedo->display($c);
					?>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="picture-block minheight-490 text-center">
					<div class="picture-frame zindex10 rotate-m15deg right-m15 top170">
						<div class="frame-bg"></div>
						<?php
							$area_image_holder_wwd3 = new Area("image holder 3");
							$area_image_holder_wwd3 ->display($c);
						?>
					</div>
					<div class="picture-frame zindex-11 rotate-15deg left-m15 top100 hidden-xs">
						<div class="frame-bg"></div>
						<?php
							$area_image_holder_wwd4 = new Area("image holder 4");
							$area_image_holder_wwd4 ->display($c);
						?>
					</div>
				</div>
			</div>
		
		
			<div class="fly scrib-rose2 wsd-rose3"></div>
			<div class="fly paint22 mobile-hide"></div>

      <div class="fly scrib-pomegranate-wd sp-wd hidden-xs hidden-md hidden-lg"></div>
			<div class="fly scrib-parsley2 sp2 hidden-sm"></div>
		</div> <!-- End first section intro -->
		
		
		<!-- Second section -->
		
		<a name="EventsWeCater" class="just-for-link"></a>
		<div class="row wwd-block-2">
			<div class="col-md-6 hidden-xs hidden-sm">
				<div class="text-center">
					<div class="picture-frame just-for-frame rotate-15deg eventwc-pos-1 ">
						<div class="frame-bg"></div>
						<?php
							$area_image_holder_wwd1 = new Area("image holder 1");
							$area_image_holder_wwd1 ->display($c);
						?>
					</div>
					<div class="picture-frame just-for-frame rotate-m20deg eventwc-pos-2 left-m60">
						<div class="frame-bg"></div>
						<?php
							$area_image_holder_wwd2 = new Area("image holder 2");
							$area_image_holder_wwd2 ->display($c);
						?>
					</div>
				</div>
			</div>
			<div class="col-md-6">
		    <?php
					$area_events_cater = new Area("events_we_cater");
					$area_events_cater->display($c);
				?>
				<div class="events">
					<a name="EventsWeCater"></a>
					<div class="text black">
						<?php
							$area_events = new Area("Events_covered");
							$area_events->display($c);
						?>
      			<div class="fly scrib-parsley3 posnew hidden-xs"></div>
					</div>
				</div>
			</div>  
			<div class="col-md-6 hidden-md hidden-sm hidden-lg">
				<div class="text-center">
					<div class="picture-frame just-for-frame rotate-15deg eventwc-pos-1 ">
						<div class="frame-bg"></div>
						<?php
							$area_image_holder_wwd1 = new Area("image holder 1");
							$area_image_holder_wwd1 ->display($c);
						?>
					</div>
					<div class="picture-frame just-for-frame rotate-m20deg eventwc-pos-2 left-m60">
						<div class="frame-bg"></div>
						<?php
							$area_image_holder_wwd2 = new Area("image holder 2");
							$area_image_holder_wwd2 ->display($c);
						?>
					</div>
				</div>
			</div>      
      <div class="fly paint23 mobile-hide"></div>
      <div class="fly scrib-pomegranate-wd sp-wd hidden-sm"></div>
		</div> <!-- End second section, Events we cater -->	

	<!-- Cuisine selection, and Food Presentation -->

	<a name="CuisinesWeOffer" class="just-for-link"></a>
	<div class="row">
		<div class="col-md-6">
      <div class="cuisines">
		    <?php
					$area_events_cater = new Area("cusine_heading");
					$area_events_cater->display($c);
				?>
			</div>
		</div>
		<div class="col-md-6">
      <div class="fly paint32 hidden-xs"></div>
		</div>
	</div>
	
  	<a name="FoodPresentation" class="just-for-link"></a>
    <div class="row mar-t-80">
      <div class="col-md-6 wwd-block-1 food-prsnt hidden-xs">
        <div class="text black">
          <?php
						$area_cuisines = new Area("cuisines_block");
						$area_cuisines->display($c);
					?>       
					<!-- 24 Nov -->
        	<div class="picture-frame zindex10 rotate-5deg top30">
            <div class="frame-bg"></div>
              <?php
								$area_image_holder_cusine = new Area("image holder cusine");
								$area_image_holder_cusine ->display($c);
							?>
      		</div>
        </div>
        <div class="divider-30"></div>
      </div>
      <div class="col-md-6 wwd-block-1 hidden-lg hidden-sm  hidden-md">
      		<div class="text black">
      			<?php
							$area_cuisines = new Area("cuisines_block");
							$area_cuisines->display($c);
						?> 
      		</div>
        	<div class="picture-frame zindex10 rotate-5deg top30">
            <div class="frame-bg"></div>
              <?php
								$area_image_holder_cusine = new Area("image holder cusine");
								$area_image_holder_cusine ->display($c);
							?>
      		</div>
        <div class="divider-30"></div>
      </div>
        <!-- Block 1 -->
        
      <div class="col-md-6 wwd-block-2">
        <div class="styles-settings">
          <?php
						$area_events_settings = new Area("Events_covered_style_settings");
						$area_events_settings->display($c);
					?>
        </div>  
        <div class="fly scrib1 scrib2-2 mobile-hide"></div>
      	<div class="divider-50"></div>
      </div>
    </div>
        <!-- Block 2 -->

    <!-- Row 1-->
    <a name="DayOfYourEvent" class="just-for-link"></a>		
	  <div class="row wwd-block-3">
	  	<div class="col-md-6 hidden-xs hidden-md hidden-lg">
    		<div class="picture-block text-center minheight-410">        
        	<div class="picture-frame rotate-15deg zindex11 right-m15 top175">
            <div class="frame-bg"></div>
              
					    <img src="/application/files/8314/8643/1377/tablesetting.JPG" alt="tablesetting.JPG" width="308" height="320" class="ccm-image-block img-responsive bID-51292">

					          </div>
					          <div class="picture-frame rotate-m10deg zindex10 top20">
					            <div class="frame-bg"></div>
					            
					    <img src="/application/files/2814/8643/1599/chefinkitchen_with_diningtable.jpg" alt="chefinkitchen_with_diningtable.jpg" width="308" height="320" class="ccm-image-block img-responsive bID-51293">

					          </div>      
					  				<img src="images/polaroid_wd_4.png" alt="What happens on the day" class="img-responsive hidden">
					        </div>
								</div>
      <div class="col-md-6">
        <div class="text-block">
      		<?php
						$area_theday = new Area("cuisines_the_day");
						$area_theday->display($c);
					?>
      	</div>
			</div>
      <div class="col-md-6 hidden-sm">
    		<div class="picture-block text-center minheight-410">        
        	<div class="picture-frame rotate-15deg zindex11 right-m15 top175">
            <div class="frame-bg"></div>
              <?php
								$area_image_holder_wwd5 = new Area("image holder 5");
								$area_image_holder_wwd5 ->display($c);
							?>
          </div>
          <div class="picture-frame rotate-m10deg zindex10 top20">
            <div class="frame-bg"></div>
            <?php
							$area_image_holder_wwd6 = new Area("image holder 6");
							$area_image_holder_wwd6 ->display($c);
						?>
          </div>      
  				<img src="images/polaroid_wd_4.png" alt="What happens on the day" class="img-responsive hidden">
        </div>
			</div>   
	    <div class="fly doscrib6 mobile-hide"></div>
	    <div class="fly doscrib7 mobile-hide"></div>
	    <div class="fly scrib-olives"></div>
	    <div class="divider-50"></div>  
    </div>
    <!-- Block 3 -->
    
    <!-- Row 2 -->
	  <a name="AdditionalHire" class="just-for-link"></a>
	  <div class="row wwd-block-4">
    	<div class="col-md-6 hidden-xs">
    		<div class="picture-block text-center minheight-410 hidden-xs">        
        	<div class="picture-frame rotate-m15deg zindex10 right-m15 top20">
            <div class="frame-bg"></div>
              <?php
								$area_image_holder_7 = new Area("image holder 7");
								$area_image_holder_7 ->display($c);
							?>
          </div>
	        <div class="picture-frame rotate-15deg left-m15 zindex10 top90">
	          <div class="frame-bg"></div>
		          <?php
								$area_image_holder_8 = new Area("image holder 8");
								$area_image_holder_8 ->display($c);
							?>
	        </div>
	        <img src="images/polaroid_wd_5.png" alt="Additional Services" class="img-responsive hidden">
	      </div>
        <div class="fly doscrib8 mobile-hide hidden-sm"></div>
        <div class="divider-50 hidden-xs hidden-sm"></div>
        <div class="divider-50 hidden-xs hidden-sm"></div>
    	</div>

    	<div class="col-md-6">
  		  <div class="text-block">
    			<?php
						$area_other_text_2 = new Area("cuisines_addtional_services");
						$area_other_text_2->display($c);
					?>
      	</div>
    	</div>

    	<div class="col-md-6 hidden-sm hidden-md hidden-lg">
    		<div class="picture-block text-center minheight-410">        
        	<div class="picture-frame rotate-m15deg zindex10 right-m15 top20">
            <div class="frame-bg"></div>
              <?php
								$area_image_holder_7 = new Area("image holder 7");
								$area_image_holder_7 ->display($c);
							?>
          </div>
	        <div class="picture-frame rotate-15deg left-m15 zindex10 top90">
	          <div class="frame-bg"></div>
		          <?php
								$area_image_holder_8 = new Area("image holder 8");
								$area_image_holder_8 ->display($c);
							?>
	        </div>
	        <img src="images/polaroid_wd_5.png" alt="Additional Services" class="img-responsive hidden">
	      </div>
        <div class="fly doscrib8 mobile-hide hidden-sm"></div>
        <div class="divider-50 hidden-xs hidden-sm"></div>
        <div class="divider-50 hidden-xs hidden-sm"></div>
    	</div>
    </div>
    
    
    <a name="DietaryRequirements" class="just-for-link"></a>
    <div class="row">
    	<div class="col-md-6">
        <div class="text-block">
          <?php
						$area_other_text = new Area("cuisines_dietary_req");
						$area_other_text->display($c);
					?>
				</div>
    	</div>

    	<div class="col-md-6">
				<div class="fly scrib-lavender"></div>
    	</div>
    </div>

    
    <a name="Locations" class="just-for-link"></a>
    <div class="row">
      <div class="col-md-6 mar-t-200 hidden-sm">
				<img src="/application/themes/thk/images/do-hand-map.png" alt="Drawn Map" class="fly doscrib3 only-desktop hidden-xs">
        <div class="fly doscrib4 botauto hidden-xs"></div>
        <div class="vdsec-divider newpos hidden-sm"></div>
      </div>

      <div class="col-md-6">
        <div class="locations">
					<?php
						$area_events_cater = new Area("locations_heading");
						$area_events_cater->display($c);
					?>
                    <div class="text black">
                        <?php
							$area_locations = new Area("location");
							$area_locations->display($c);
						?>
					</div>			
				</div>    	
      </div>		
  
    </div>

		
		<!-- Row 3 -->

		<a name="LoveWhatYouSee?" class="just-for-link"></a>
		<div class="row wwd-block-5 mobile-hide hidden-sm hidden-xs">
			<div class="col-md-6">
				<div class="text-block">
					<?php
						$area_textblock = new Area("what_you_see");
						$area_textblock->display($c);
					?>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="divider-15"></div>
				<div class="picture-block text-center minheight-410">
        	<div class="picture-frame rotate-m15deg zindex10 right-m15 top20">
            <div class="frame-bg"></div>
            <?php
							$area_image_holder_9 = new Area("image holder 9");
							$area_image_holder_9 ->display($c);
						?>
          </div>
          <div class="picture-frame rotate-10deg left-m15 zindex10 top50">
            <div class="frame-bg"></div>
            <?php
							$area_image_holder_10 = new Area("image holder 10");
							$area_image_holder_10 ->display($c);
						?>
          </div>
          <div class="fly scrib-mint hidden-xs"></div>
  			</div>
			</div>
		</div><!-- Block 5 -->
		<div class="row wwd-block-5 hidden-md visible-sm visible-xs">
			<div class="col-md-6">
				<div class="text-block">
					<?php
						$area_textblock = new Area("what_you_see");
						$area_textblock->display($c);
					?>
				</div>
			</div>
			<div class="col-md-6">
				<div class="divider-15"></div>
				<div class="picture-block text-center minheight-410">
        	<div class="picture-frame rotate-m15deg zindex10 right-m15 top20">
            <div class="frame-bg"></div>
            <?php
							$area_image_holder_9 = new Area("image holder 9");
							$area_image_holder_9 ->display($c);
						?>
          </div>
          <div class="picture-frame rotate-15deg left-m15 zindex10 top50">
            <div class="frame-bg"></div>
            <?php
							$area_image_holder_10 = new Area("image holder 10");
							$area_image_holder_10 ->display($c);
						?>
          </div>
					<div class="fly scrib-mint"></div>
  			</div>
			</div>
		</div><!-- Block 5 -->
		<!-- Row 4 -->
		<div class="row wwd-block-6">
			<div class="block-end-msg col-md-12 text-center">
				<?php
					$area_navigate_to = new Area("navigate_to");
					$area_navigate_to->display($c);
				?>
				<div class="clearfix text-center">
					<img src="/application/themes/thk/images/sh-seprator_white.png" alt="line Seperator" class="img-responsive center-block" />
				</div>
			</div>
		</div><!-- Block 6 -->
		
	</div> <!-- end first container -->

	</div><!-- /.container -->
		<?php  $this->inc('elements/footer.php'); ?>
</div>