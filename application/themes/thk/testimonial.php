<?php
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php'); ?>

<div class="ourClients-page">
	<section class="ourClients-section">
		<div class="container"> 
			<div class="row testim-tsec">
				<div class="col-sm-12 col-md-11 col-md-offset-1">
					<?php 
						$area_testimoinalPage_intro = new Area('Testimonial_page_intro');
						$area_testimoinalPage_intro->display($c);
					?>
					<div class="fly scrib-olives mobile-hide"></div>
				</div>
				
			</div>
		</div>
		<div class="divider-50"></div>
		<div class="container">
		 	<a name="CateringTestimonial" class="just-for-link"></a>
			<?php 
				$area_testimoinals = new Area('Testimonials');
				$area_testimoinals->display($c);
			?>
		 	<a name="BrandingTestimonial" class="just-for-link"></a>
			<?php 
				$area_branding_testimoinals = new Area('Branding Testimonials');
				$area_branding_testimoinals->display($c);
			?>
		<div class="divider-15"></div>
		  <div class="row">
			<div class="block-end-msg col-md-12 text-center">
            	<div class="clearfix text-center">
              		<img src="/application/themes/thk/images/sh-seprator_white.png" class="img-responsive center-block" />
              	</div>  
            </div>
   		</div><!-- Block Seprator -->
		</div>
	</section>
	

<?php  $this->inc('elements/footer.php'); ?>
</div>