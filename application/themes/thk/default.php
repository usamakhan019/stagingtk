<?php
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php'); ?>
<div class="blog-page">
<div class="container">
	<div class="row">
		<div class="col-md-12">
		<?php
		$a = new Area('Main');
		$a->enableGridContainer();
		$a->display($c);
		?>
		</div>
	</div>
</div>
</div>

<?php  $this->inc('elements/footer.php'); ?>
