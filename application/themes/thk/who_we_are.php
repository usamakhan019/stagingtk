<?php
	defined('C5_EXECUTE') or die("Access Denied.");
	$this->inc('elements/header.php'); 
?>

<div class="whoweare-page">
	<div class="container">

		<a name="theHampSteadKitchen" class="just-for-link"></a>
		<div class="row wwa-block-1 mar-t-75">

			<div class="col-md-6">
				<?php
					$area_intro = new Area('who_we_are_intro');
					$area_intro->display($c);
				?>
			</div>

			<div class="col-md-6">
				<div class="picture-block text-center minheight-410">
					<div class="picture-frame rotate-m15deg zindex11 top120 right-m15">
						<div class="frame-bg"></div>
						<?php
							$area_image_1 = new Area('image area 1');
							$area_image_1->display($c);
						?>
					</div>
					<div class="picture-frame rotate-7deg zindex10 top20 left-m15 hidden-xs">
						<div class="frame-bg"></div>
						<?php
							$area_image_2 = new Area('image area 2');
							$area_image_2->display($c);
						?>
					</div>
				</div>
			</div>

			<div class="fly scrib-rose1"></div>
			<div class="fly scrib-pomegranate-wa zindex11"></div>

		</div>


		<a name="ApproachToFood" class="just-for-link"></a>
		<div class="row wwa-block-2 hidden-sm hidden-xs">
			<div class="divider-50"></div>

			<div class="col-md-6">
				<div class="picture-block text-center">
					<div class="picture-frame rotate-7deg zindex10 top20">
						<div class="frame-bg"></div>
						<?php
							$area_intro_2 = new Area('Block2');
							$area_intro_2->display($c);
						?>
					</div>
					<img src="images/polaroid_wa2.png" alt="Our Approach To Food.." class="img-responsive hidden">
				</div>
			</div>

			<div class="col-md-6">
				<div class="text-block">
					<?php
						$area_block3 = new Area('who_we_cook_for_block');
						$area_block3->display($c);
					?>
				</div>
			</div>

			<div class="fly scrib-parsley1 mobile-hide hidden-sm"></div>
			<div class="fly yellow-sticky mobile-hide hidden-sm"></div>

		</div>

		<a name="ApproachToFood" class="just-for-link"></a>
		<div class="row wwa-block-2 visible-xs visible-sm hidden-md">

			<div class="col-md-6">
				<div class="text-block">
					<?php
						$area_block3 = new Area('who_we_cook_for_block');
						$area_block3->display($c);
					?>
				</div>
			</div>

			<div class="col-md-6">
				<div class="picture-frame rotate-7deg zindex10 top20">
					<div class="frame-bg"></div>
					<?php
						$area_intro_2 = new Area('Block2');
						$area_intro_2->display($c);
					?>
				<div class="fly scrib-parsley-for-ipad hidden-xs"></div>
				</div>
			</div>

			<div class="fly yellow-sticky hidden-xs hidden-sm"></div>

		</div>

		<a name="AttentionToDetail" class="just-for-link"></a>
		<div class="row wwa-block-3">

			<div class="col-md-6">
				<div class="text-block">
					<div class="divider-50 hidden-xs"></div>
					<?php
						$area_block4 = new Area('getstarted_block');
						$area_block4->display($c);
					?>
				</div>
			</div>

			<div class="col-md-6">
				<div class="picture-block text-center minheight-410">

					<div class="picture-frame rotate-10deg zindex11 right-m42 top200 hidden-xs">
						<div class="frame-bg"></div>
						<?php
							$area_image_4 = new Area('image area 4');
							$area_image_4->display($c);
						?>
					</div>

					<div class="picture-frame rotate-m7deg zindex10 left-m30 top30">
						<div class="frame-bg"></div>
						<?php
							$area_image_5 = new Area('image area 5');
							$area_image_5->display($c);
						?>
					</div>

				</div>
			</div>

			<div class="fly red hidden-sm hidden-xs"></div>
			<div class="fly paint4 hidden-xs"></div>
			<div class="fly doscrib13 hidden-xs"></div>
			<div class="vdsec-divider hidden-md hidden-lg hidden-sm hidden-xs"></div>

		</div>

		<a name="HowWeStarted" class="just-for-link"></a>
		<div class="row wwa-block-4 hidden-sm hidden-xs">

			<div class="col-md-6">
				<div class="picture-block minheight-530 text-center">
					<div class="picture-frame-headchef rotate-7deg top120">
						<div class="frame-bg"></div>
						<?php
							$area_block5 = new Area('images_who_we_are_block');
							$area_block5->display($c);
						?>
					</div>
					<img src="/application/files/4014/8292/1628/head-chef-pic.png" class="img-responsive hidden" alt="Saima - Head Chef & Founder.">
				</div>

				<?php if ($c->isEditMode()) { ?> <div> <?php } else { ?> <div class="hidden_image"> <?php }
						$area_block10 = new Area('images_who_we_are_block_2');
						$area_block10->display($c);
					?>
				</div>
			</div>
			
			<div class="col-md-6">
				<?php
					$area_block5 = new Area('finance_food_block');
					$area_block5->display($c);
				?>

				<?php if ($c->isEditMode()) { ?> <div> <?php } else { ?> <div class="hidden_text"> <?php }
						$area_block5 = new Area('finance_food_block_hidden');
						$area_block5->display($c);
					?>
				</div>
				<p class="p_more"><a href="#" class="more">Read more</a></p>
            </div>
	
			<div class="fly scrib2 mobile-hide"></div>
			<div class="fly scrib-parsley2 mobile-hide"></div>
			<div class="fly scrib-anjeer mobile-hide"></div>			
		</div>

		<div class="row wwa-block-4 hidden-md visible-sm visible-xs">
			<div class="col-md-6">
				<?php
					$area_block5 = new Area('finance_food_block');
					$area_block5->display($c);
				?>
				<?php if ($c->isEditMode()) { ?> <div> <?php } else { ?> <div class="hidden_text"> <?php }
					$area_block5 = new Area('finance_food_block_hidden');
					$area_block5->display($c);
				?>
				</div>
				<p class="p_more"><a href="#" class="more">Read more</a></p>
			</div>

			<div class="col-md-6">
				<div class="picture-block minheight-530 text-center">
					<div class="picture-frame-headchef rotate-7deg top120">
						<div class="frame-bg"></div>
						<?php
							$area_block5 = new Area('images_who_we_are_block');
							$area_block5->display($c);
						?>
					<div class="fly scrib-parsley3  hidden-xs"></div>
					</div>
				</div>
				<?php if ($c->isEditMode()) { ?> <div> <?php } else { ?> <div class="hidden_image"> <?php }
						$area_block10 = new Area('images_who_we_are_block_2');
						$area_block10->display($c);
					?>
				</div>
			</div>

			<div class="fly scrib2 hidden-xs hidden-xs"></div>
			<div class="fly red2 hidden-lg hidden-xs"></div>
			<div class="fly scrib-anjeer hidden-xs hidden-sm"></div>
			
		</div>

		<a name="OurClientsEvents" class="just-for-link"></a>
		<div class="divider-50"></div>
		<div class="row wwa-block-5">

			<div class="col-md-6">
				<div class="text-block">
					<?php
						$area_block7 = new Area('achievements');
						$area_block7->display($c);
					?>
				</div>
			</div>

			<div class="col-md-6">
				<div class="picture-block text-center">
					<div class="picture-block text-center minheight-410">

						<div class="picture-frame rotate-m15deg zindex11 top120 right-m15 hidden-xs">
							<div class="frame-bg"></div>
							<?php
								$area_image_7 = new Area('image area 7');
								$area_image_7->display($c);
							?>
						</div>

						<div class="picture-frame rotate-7deg zindex10 left-m15">
							<div class="frame-bg"></div>
							<?php
								$area_image_8 = new Area('image area 8');
								$area_image_8->display($c);
							?>
						</div>

					</div>
				</div>
			</div>

			<div class="fly scrib-pistasho"></div>

		</div>

		<a name="BrandMentoring" class="just-for-link"></a>
		<div class="row wwa-block-7 just-for-bullets">
			<div class="col-md-6 visible-sm visible-xs hidden-md hidden-lg">
				<?php
					$brandMentoring_textarea = new Area('brandMentoring_textarea');
					$brandMentoring_textarea->display($c);
				?>
			</div>

			<div class="col-md-6">
				<div class="picture-block text-center">
					<div class="picture-block text-center minheight-410">
						<div class="picture-frame rotate-m15deg zindex11 top50">
							<div class="frame-bg"></div>
							<?php
								$brandMentoring_polariod = new Area('brandMentoring_polariod');
								$brandMentoring_polariod->display($c);
							?>
						</div>
						<div class="double-aubergine hidden-xs"></div>
					</div>
				</div>
			</div>

			<div class="col-md-6 hidden-sm hidden-xs">
				<?php
					$brandMentoring_textarea = new Area('brandMentoring_textarea');
					$brandMentoring_textarea->display($c);
				?>
			</div>
		</div>

		<a name="CreatingYourBrand" class="just-for-link"></a>
        <div class="row wwa-block-5">

            <div class="col-md-6">
                <div class="text-block">
                    <?php
                        $area_block7 = new Area('Creating_your_brand');
                        $area_block7->display($c);
                    ?>
                </div>
            </div>

            <div class="col-md-6">
                <div class="picture-block text-center">
                    <div class="picture-block text-center minheight-410">
                        <div class="picture-frame rotate-15deg zindex10 top50">
                            <div class="frame-bg"></div>
                            <?php
                                $area_image_10 = new Area('image area 27');
                                $area_image_10->display($c);
                            ?>
                        	<div class="createyourbrand"></div>
                        </div>
                    </div>
                </div>
			</div>
			
		</div>
		
		<a name="OneDayWorkShop" class="just-for-link"></a>
		<div class="row wwa-block-7 just-for-bullets">

			<div class="col-md-6 visible-sm visible-xs hidden-md hidden-lg">
				<?php
					$area_block10 = new Area('add_New_name');
					$area_block10->display($c);
				?>
			</div>

			<div class="col-md-6">
				<div class="picture-block text-center">
					<div class="picture-block text-center minheight-410">

						<div class="picture-frame rotate-m15deg zindex11 top50">
							<div class="frame-bg"></div>
							<?php
								$area_image_9 = new Area('image area 32');
								$area_image_9->display($c);
							?>
						</div>
						<div class="just-text"></div>
						<div class="for-leaves hidden-xs"></div>
					</div>
				</div>
			</div>

			<div class="col-md-6 hidden-sm hidden-xs">
				<?php
						$area_block10 = new Area('add_New_name');
						$area_block10->display($c);
				?>
			</div>

		</div>

		<a name="dayWorkShopAndPackages" class="just-for-link"></a>
		<div class="row wwa-block-5">
			<div class="col-md-6">
				<div class="text-block">
					<?php
						$area_block7 = new Area('testmonials_area');
						$area_block7->display($c);
					?>
				</div>
			</div>
			<div class="col-md-6">
				<div class="picture-block text-center">
					<div class="picture-block text-center minheight-410">
						<div class="picture-frame rotate-15deg zindex10 top50">
							<div class="frame-bg"></div>
							<?php
								$area_image_10 = new Area('image area 33');
								$area_image_10->display($c);
							?>
							<div class="for-pom-double hidden-xs"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<a name="TestimonialBrand" class="just-for-link"></a>
		<section class="testimonial-section branding just-for-link">
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center">
						<?php
							$imSlider_area = new Area('slider_area_wo_we_are');
							$imSlider_area->display($c);
						?>
					</div>
				</div>
			</div>
		</section>
		
		<div class="row wwa-block-8">
			<div class="block-end-msg col-md-12 text-center">
				<?php
					$area_block13 = new Area('what_we_do');
					$area_block13->display($c);
				?>
				<div class="clearfix text-center">
					<img src="/application/themes/thk/images/sh-seprator_white.png" alt="section seperator" class="img-responsive center-block"/>
				</div>
			</div>
		</div>

		<div class="fly scrib1 mobile-hide hidden"></div>
		<div class="fly yellow-sticky hidden"></div>
		<div class="fly paint1 mobile-hide hidden"></div>
		<div class="fly paint3 mobile-hide hidden"></div>
							

	</div>

	<div class="fly scrib1 mobile-hide"></div>
	<div class="fly doscrib5 mobile-hide"></div>
	<?php  $this->inc('elements/footer.php'); ?>


</div>