<?php defined('C5_EXECUTE') or die("Access Denied.");
$view->inc('elements/header.php');
?>

<div class="blog-page subscription-pg">
  <div class="container">
    <div class="row">
      <div class="col-md-5 wwd-block-1 hidden-sm hidden-xs">
        <div class="events">
          <div class="text black">
            <?php
              $subscription_image_area = new Area('subscription_image_area');
              $subscription_image_area->display($c);
            ?>
            <div class="fly scrib-rose3 hidden-sm hidden-xs"></div>
          </div>
        </div>
      </div>
      <!-- <div class="col-md-5 wwd-block-2 hidden-sm hidden-xs">
        <div class="unsub-note">
          <div class="divider-15 mobile-hide"></div>
          <div class="events">
            <div class="text black">
              <?php
                $subscription_image_area = new Area('subscription_image_area');
                $subscription_image_area->display($c);
              ?>
            </div>
          </div>
        </div>
      </div> -->
      <div class="col-md-7 wwd-block-1">
        <div class="unsub-note">
          <?php
            $subscription_header_area = new Area('subscription_content_area');
            $subscription_header_area->display($c);
          ?>
          <div class="col-md-5 col-sm-5 col-xs-10">
            <?php if ($c->isEditMode()) { ?>
            <div id="subpage-cm" style="display:block;">
              <div class="divider-50"></div>
            <?php }else{ ?>
            <div id="subpage-cm">
              <div class="divider-50"></div>
            <?php } ?>

              <?php
                $subscription_form_cm = new Area('subscription_form_confirmation_message');
                $subscription_form_cm->display($c);
              ?>
            </div>
            <div id="subpage-form" class="unsub-note subpage subscription-form">
              <div class="divider-30"></div>
              <?php
                  $subscription_form_area = new Area('subscription_form_area');
                  $subscription_form_area->display($c);
              ?>
            </div>
          </div>
          <div class="col-sm-offset-1 col-md-5 col-sm-5 hidden-xs">


 	    <div class="text-center">
                <div class="picture-frame-subscr rotate-25deg top30">
                    <div class="frame-bg"></div>
           		         
           		 <?php
              			$subscription_image2_area = new Area('subscription_image2_area');
              			$subscription_image2_area->display($c);
            		?>

                </div>
                <div class="fly scrib-subscr-leafs hidden-xs"></div>
 	    </div>
            
            
        	<img src="/application/files/8414/6210/3841/sub-saima.png" alt="saima" width="258" height="330" class="ccm-image-block img-responsive bID-42970 hidden" title="Saima">





          </div>
          <div class="fly scrib-split-pomegrante hidden-xs hidden-sm"></div>
	  <div class="divider-50"></div>
        </div>
      </div>
    </div>
    <div class="divider-50"></div>
    <div class="row element-row">
      <div class="col-md-12">
        <?php
          $area_blog_posts_slider = new Area("posts slider");
          $area_blog_posts_slider->display($c);
        ?>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 clearfix text-center">
        <img src="/application/themes/thk/images/sh-seprator_white.png" class="img-responsive center-block" />
      </div>
      <div class="divider-50"></div>
    </div>
  </div>

<?php $view->inc('elements/footer.php'); ?>
