<?php
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php'); 
$page = Page::getCurrentPage();
?>
			
<div id="instafeed" style="display : none;"></div>

<div  class="blog-page">
	<div class="container"> 
		<div class="row">
			<div class="col-md-8 blog-page-top">
				<div class="row">
					<div class="col-md-7 col-sm-7 col-xs-12">
						<h1 class="mar-t0"><?php echo ($page->getCollectionName()) ?></h1>	
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">	
						<?php 
							$area_blog_header = new Area("Search Blog category page");
							$area_blog_header->display($c);
						?>
					</div>	
				</div>
				<div class="row">
					<div class="col-md-12">
						<p><?php echo($page->getCollectionDescription()) ?></p>
						<div class="divider-50"></div>
					</div>
				</div>
				
				
				<div class="row blog-row category-listing">
					<div class="col-md-12 col-sm-12">
					<?php 
						$area_blog_categories = new Area("category post listing");
						$area_blog_categories->display($c);
					?>
					</div>	
				</div>
			</div><!-- Col Left -->
			<div class="col-md-4 blog-right-col  blogcate_sub">
				<div class="blog-subscibe col-sm-6 col-md-12 mobile-hide">
						<?php 
								$area_log_subscribe= new Area("Blog subscription form");
								$area_log_subscribe->display($c);
						?>
					<div class="divider-50"></div>
				</div>
				<!-- Blog Subscribe -->
				
				<div class="blog-cat-list col-sm-6 col-md-12">
					<?php 
							$area_categoriesListing = new Area("category posts listing");
							$area_categoriesListing->display($c);
					?>
				</div>
				<div class="divider-50"></div>
				<div class="blog-morePosts">	
					<?php 
						$area_blog_posts = new Area("latest Blog Posts");
						$area_blog_posts->display($c);
					?>
				</div>
				<div class="fly scrib-rose3 mobile-hide"></div>
				  <div class="fly doscrib11 mobile-hide"></div>
				<div class="fly scrib-pomegranate-3 mobile-hide"></div> 
				<div class="fly white-paper mobile-hide hidden-sm"></div> 
			</div><!-- Col 4 --><!-- Col Right -->
		</div><!-- /.row -->
	</div><!-- /.container -->
	
	<div class="container"> 
		 <div class="row element-row">
        <div class="fly doscrib4 mobile-hide"></div>
        <div class="fly scrib-parsley4 mobile-hide"></div>
    </div><!-- /.row -->
        
        
        

    <div class="row">
        <div class="divider-50"></div>
        <div class="col-md-3 col-md-offset-1">
			<?php 
				$area_video = new Area("Video_area2");
				$area_video->display($c);
			?>
        </div>
        <div class="col-md-8">
        	<div class="blog-video-contianer">
                
				<?php 
					$area_video = new Area("Video_frame2");
					$area_video->display($c);
				?>
               
            </div>
        </div>
    </div><!-- /.row -->
	
	 
    <div class="row instagram-feed">
    	<div class="col-sm-4 text-center mobile-hide hidden-sm"><h2><img src="/application/themes/thk/images/h3_top_x2.png" width="100%" alt="Instagram feed sideline"  height="18"></h2></div>
        <div class="col-sm-12 col-md-4 text-center">
			<?php 
					$area_video = new Area("Instagram feed header 3");
					$area_video->display($c);
			?>
			
		</div>
        <div class="col-sm-4 text-center mobile-hide hidden-sm"><h2><img src="/application/themes/thk/images/h3_top_x2.png" alt="Instagram feed sideline" width="100%" height="18"></h2></div>
		<div class="divider-30"></div>
		<div class="col-md-12">
			<div id="carousel-instagramfeed" class="carousel slide" data-ride="carousel">                
                  <div class="carousel-inner" role="listbox" id="carousel_container">
					 
				  </div> <!-- Inner Carousel -->
				  <!-- Controls -->
                  <a class="left carousel-control" href="#carousel-instagramfeed" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#carousel-instagramfeed" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
			</div> <!-- carousel End -->
		</div>
    </div><!-- Instagram /.row -->

	
    
<div class="block-end-msg col-md-12 text-center">    
        <div class="clearfix text-center">
            <img class="img-responsive center-block" alt="line seperator" src="/application/themes/thk/images/sh-seprator_white.png">
        </div>  
</div>
</div><!-- /.container -->



<?php  $this->inc('elements/footer.php'); ?>
</div>