<?php
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php'); ?>
<div class="whatwedo-page">
	<div class="container">
    <!-- First Row -->
		<a name="SocialBubbling" class="just-for-link"></a>
    <div class="row wwd-block-1 mar-t-75">
      <div class="col-md-6">
        <div class="text-block">
          <?php
            $area_01_01 = new Area("social dining content area 01");
            $area_01_01->display($c);
          ?>
        </div>
      </div>
			
			<div class="col-md-6">
				<div class="picture-block minheight-490 text-center">
					<div class="picture-frame zindex10 rotate-m15deg right-m15 top170">
						<div class="frame-bg"></div>
						<?php
							$area_01_02 = new Area("image holder 3");
							$area_01_02 ->display($c);
						?>
					</div>
					<div class="picture-frame zindex-11 rotate-15deg left-m15 top100 hidden-xs">
						<div class="frame-bg"></div>
						<?php
							$area_image_holder_wwd4 = new Area("image holder 4");
							$area_image_holder_wwd4 ->display($c);
						?>
					</div>
				</div>
			</div>
		
		
			<div class="fly scrib-rose2 wsd-rose3"></div>
			<div class="fly paint22 mobile-hide"></div>

      <div class="fly scrib-pomegranate-wd sp-wd hidden-xs hidden-md hidden-lg"></div>
			<div class="fly scrib-parsley2 sp2 hidden-sm"></div>
		</div>
		
		
		<!-- Second Row -->
		<a name="BubbleBanquets" class="just-for-link"></a>
		<div class="row wwd-block-2">
			<div class="col-md-6 hidden-xs hidden-sm">
				<div class="text-center">
					<div class="picture-frame just-for-frame rotate-15deg eventwc-pos-1 ">
						<div class="frame-bg"></div>
						<?php
							$area_image_holder_wwd1_test = new Area("image holder test");
							$area_image_holder_wwd1_test ->display($c);
						?>
					</div>
					<div class="picture-frame just-for-frame rotate-m20deg eventwc-pos-2 left-m60">
						<div class="frame-bg"></div>
						<?php
							$area_image_holder_wwd2 = new Area("image holder 2");
							$area_image_holder_wwd2 ->display($c);
						?>
					</div>
				</div>
			</div>
			<div class="col-md-6">
		    <?php
					$area_events_cater = new Area("events_we_cater");
					$area_events_cater->display($c);
				?>
				<div class="events">
					<a name="EventsWeCater"></a>
					<div class="text black">
						<?php
							$area_events = new Area("Events_covered");
							$area_events->display($c);
						?>
      			<div class="fly scrib-parsley3 posnew hidden-xs"></div>
					</div>
				</div>
			</div>  
			<div class="col-md-6 hidden-md hidden-sm hidden-lg">
				<div class="text-center">
					<div class="picture-frame just-for-frame rotate-15deg eventwc-pos-1 ">
						<div class="frame-bg"></div>
						<?php
							$area_image_holder_wwd1_test = new Area("image holder test");
							$area_image_holder_wwd1_test ->display($c);
						?>
					</div>
					<div class="picture-frame just-for-frame rotate-m20deg eventwc-pos-2 left-m60">
						<div class="frame-bg"></div>
						<?php
							$area_image_holder_wwd2 = new Area("image holder 2");
							$area_image_holder_wwd2 ->display($c);
						?>
					</div>
				</div>
			</div>      
      <div class="fly paint23 mobile-hide"></div>
      <div class="fly scrib-pomegranate-wd sp-wd hidden-sm"></div>
		</div> 

    <!-- Third and forth row -->
    <a name="BubbleCuisines" class="just-for-link"></a>
    <div class="row">
      <div class="wwd-block-4 col-md-6">
        <div class="cuisines">
          <?php
            $area_events_cater = new Area("cusine_heading");
            $area_events_cater->display($c);
          ?>
        </div>
      </div>
      <div class="col-md-6">
        <div class="fly paint32 hidden-xs"></div>
      </div>
    </div>
    
    <a name="WhatsIncluded" class="just-for-link"></a>
    <div class="row mar-t-80">
      <div class="col-md-6 wwd-block-1 food-prsnt hidden-xs">
        <div class="text black">
          <?php
            $area_cuisines_01 = new Area("cuisines_block");
            $area_cuisines_01->display($c);
          ?>       
          <!-- 24 Nov --> 
          <div class="picture-frame zindex10 rotate-5deg top30">
            <div class="frame-bg"></div>
              <?php
                $area_image_holder_cusine_01 = new Area("image holder cusine");
                $area_image_holder_cusine_01 ->display($c);
              ?>
          </div>
        </div>
        <div class="divider-30"></div>
      </div>
      <div class="col-md-6 wwd-block-1 hidden-lg hidden-sm  hidden-md">
          <div class="text black">
            <?php
              $area_cuisines_01 = new Area("cuisines_block");
              $area_cuisines_01->display($c);
            ?> 
          </div>
          <div class="picture-frame zindex10 rotate-5deg top30">
            <div class="frame-bg"></div>
              <?php
                $area_image_holder_cusine_01 = new Area("image holder cusine");
                $area_image_holder_cusine_01 ->display($c);
              ?>
          </div>
        <div class="divider-30"></div>
      </div>
        <!-- Block 1 -->
        
      <div class="col-md-6 wwd-block-2">
        <div class="styles-settings">
          <?php
            $area_events_settings = new Area("Events_covered_style_settings");
            $area_events_settings->display($c);
          ?>
        </div>  
        <div class="fly scrib1 scrib2-2 mobile-hide"></div>
        <div class="divider-50"></div>
      </div>
    </div>
        

    <a name="BubbleServices" class="just-for-link"></a>
    <div class="row">
      <div class="col-md-6">
        <div class="text-block">
          <?php
            $area_other_text = new Area("cuisines_dietary_req");
            $area_other_text->display($c);
          ?>
        </div>
      </div>

      <div class="col-md-6">
        <div class="fly scrib-lavender hidden-sm hidden-xs"></div>
      </div>
    </div>

      
    <a name="BubbleLocations" class="just-for-link"></a>
    <div class="row">
      <div class="col-md-6 mar-t-200 hidden-sm">
        <img src="/application/themes/thk/images/do-hand-map.png" alt="Drawn Map" class="fly doscrib3 only-desktop hidden-xs">
        <div class="fly doscrib4 botauto hidden-xs"></div>
        <div class="vdsec-divider newpos hidden-sm"></div>
      </div>

      <div class="col-md-6">
        <div class="locations">
          <?php
            $area_events_cater = new Area("locations_heading");
            $area_events_cater->display($c);
          ?>
                    <div class="text black">
                        <?php
              $area_locations = new Area("location");
              $area_locations->display($c);
            ?>
          </div>			
        </div>    	
      </div>		

    </div>

	  <a name="TheNewNormal" class="just-for-link"></a>
    <div class="row wwd-block-5 mobile-hide hidden-sm hidden-xs">
      <div class="col-md-6">
        <div class="text-block">
          <?php
            $area_textblock_01 = new Area("what_you_see");
            $area_textblock_01->display($c);
          ?>
        </div>
      </div>
      
      <div class="col-md-6">
        <div class="divider-15"></div>
        <div class="picture-block text-center minheight-410">
          <div class="picture-frame rotate-m15deg zindex10 right-m15 top20">
            <div class="frame-bg"></div>
            <?php
              $area_image_holder_9 = new Area("image holder 9");
              $area_image_holder_9 ->display($c);
            ?>
          </div>
          <div class="picture-frame rotate-10deg left-m15 zindex10 top50">
            <div class="frame-bg"></div>
            <?php
              $area_image_holder_10 = new Area("image holder 10");
              $area_image_holder_10 ->display($c);
            ?>
          </div>
          <div class="fly scrib-mint hidden-xs"></div>
        </div>
      </div>
    </div>
    <div class="row wwd-block-5 hidden-md visible-sm visible-xs">
      <div class="col-md-6">
        <div class="text-block">
          <?php
            $area_textblock_01 = new Area("what_you_see");
            $area_textblock_01->display($c);
          ?>
        </div>
      </div>
      <div class="col-md-6">
        <div class="divider-15"></div>
        <div class="picture-block text-center minheight-410">
          <div class="picture-frame rotate-m15deg zindex10 right-m15 top20">
            <div class="frame-bg"></div>
            <?php
              $area_image_holder_9 = new Area("image holder 9");
              $area_image_holder_9 ->display($c);
            ?>
          </div>
          <div class="picture-frame rotate-15deg left-m15 zindex10 top50">
            <div class="frame-bg"></div>
            <?php
              $area_image_holder_10 = new Area("image holder 10");
              $area_image_holder_10 ->display($c);
            ?>
          </div>
          <div class="fly scrib-mint"></div>
        </div>
      </div>
    </div>
    <div class="row wwd-block-6">
      <div class="block-end-msg col-md-12 text-center">
        <?php
          $area_navigate_to = new Area("navigate_to");
          $area_navigate_to->display($c);
        ?>
        <div class="clearfix text-center">
          <img src="/application/themes/thk/images/sh-seprator_white.png" alt="line Seperator" class="img-responsive center-block" />
        </div>
      </div>
    </div>	
	</div>
</div>
		<?php  $this->inc('elements/footer.php'); ?>
</div>