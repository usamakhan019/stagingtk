﻿<?php
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php');
?>

<div class="blog-page">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12 text-center">
				<div class="unsub-note">
					<h2>Boo Hoo, Sniff Sniff...</h2>
					<img src="/application/themes/thk/images/email-img_03.png" width="290">
					<div class="divider-15"></div>
					<p>We are sad to see you go, but its totally cool with us.<br/>
					 We know the feeling, sometimes there are just too many emails & very little time to read through them.</p>
					<p>Do keep in touch from time to time, so you don’t miss out on cool social, events, recipes & much more.</p>
					<p>Merci, grazie, shukran & thanks for joining us for the ride. Hope to see you again soon, if not its been nice knowing you.</p>
					<p>Take care now</p>
					<p><img src="/application/files/8514/3418/6687/saima_x2_sign3.png" width="130" ></p>
					<div class="vdsec-divider mobile-hide"></div>
				</div>
				<div class="divider-50"></div>
			</div>
		</div>
	</div>
	<div class="clearfix text-center">
		<img src="/application/themes/thk/images/sh-seprator_white.png" class="img-responsive center-block" />
	</div>




<?php  $this->inc('elements/footer.php'); ?>
</div>