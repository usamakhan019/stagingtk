<?php defined('C5_EXECUTE') or die("Access Denied.");

if(!User::IsLoggedIn()){
	if((strtoupper($c->getCollectionName()) != "HOME")
	&& (strtoupper($c->getCollectionName()) != "OUR BLOG")){?>
  		<script type="text/javascript" src="/concrete/js/jquery.js"></script>
	<?php } ?>
  <script type="text/javascript" src="/concrete/js/bootstrap/dropdown.js"></script>
  <script type="text/javascript" src="/concrete/js/bootstrap/Jslibraries.js"></script>
<?php }else{?>
  <script type="text/javascript" src="/concrete/js/bootstrap/carousel.js"></script>
  <script type="text/javascript" src="/concrete/js/bootstrap/collapse.js"></script>
<?php } ?>

<script type="text/javascript" src="/application/themes/thk/js/instafeed.min.js"></script>
<script type="text/javascript" src="/application/themes/thk/js/getInstaFeed.js"></script>
<script type="text/javascript" src="/application/themes/thk/js/jquery.scrolldepth.min.js"></script>
</div>
<script type="text/javascript">
$( document ).ready(function() {
	$.scrollDepth();
  //remove notification on scroll.
  $(window).scroll(function() {
    if ($(this).scrollTop()>300)
    {
      $("#notification-modal").fadeOut( "slow" );
    }
  });
	//carousel  on arrow keys
	$("body").keydown(function(e) {
  		if(e.keyCode == 37) { // left
    			$('.ourfooddetail-page .carousel').carousel('prev');
  		} else if(e.keyCode == 39) { // right
    			$('.ourfooddetail-page .carousel').carousel('next');
  		}
	});

	$.wait = function(ms) {
    		var defer = $.Deferred();
    		setTimeout(function() { defer.resolve(); }, ms);
    		return defer;
	};

	var $dropdown = $(".nav .dropdown-menu");

	$(document).on('click touchstart',function(e){
    		if (($dropdown.is(":visible")) && $(e.target).closest('.dropdown').length == 0){
        		$(".nav .dropdown").removeClass('open');
        		$(".navbar-fixed-bottom .dropdown a:first").blur();
        		$(".navbar-fixed-top .dropdown a:first").blur();
    		}
	});

	$('.nav .dropdown-menu').on('click touchstart',function(event){
  		event.stopPropagation();
	});

	$('.dropdown-toggle').click(function(e) {
  		e.preventDefault();
  		setTimeout($.proxy(function() {
    			if ('ontouchstart' in document.documentElement) {
      				$(this).siblings('.dropdown-backdrop').off().remove();
    			}
  		}, this), 0);
	});

	$(".btn-noteus, .give-occasion-details").click(function(e){
		$(".success_Message").hide('slow');
	    $(".letsTalk-info").hide('slow');
        $(".letsTalk-form").show('slow');
	});

	$("#letsTalkForm").click(function(e){
	    e.preventDefault();
            $(".letsTalk-info").hide('slow');
            $(".letsTalk-form").show('slow');

        });
	$(".toletsTalk_nav").click(function(e){
	    e.preventDefault();
            $('html,body').animate({scrollTop: $("#letstalk-section").offset().top},'slow');
	    $(".letsTalk-info").hide('slow');
            $(".letsTalk-form").show('slow');
        });

        $("#hideForm").click(function(){
            $(".letsTalk-info").show('slow');
            $(".letsTalk-form").hide('slow');
			$("form.miniSurveyView :input").removeClass('lt-error');
		    $("form.miniSurveyView .input6").removeClass('lt-error');
			$("#miniSurveyView")[0].reset();
        });

	$("#hideForm2").click(function(){
            $(".letsTalk-info").show('slow');
            $(".letsTalk-form").hide('slow');
			$(".success_Message").hide('slow');
        });

	$("#popupClose").click(function(){
            $("#subscription_modal").fadeOut( "slow" );
            $("#subscription_popup").addClass('subscription-popup').removeClass('subscription-popup2');
        });

  $("#notificationClose").click(function(){
      $("#notification-modal").fadeOut( "slow" );
  });

	//Subscription Forms
	var subs_url_footer = $("#subscription_form_footer").attr('action'),
	$subsformId_footer = $("#subscription_form_footer"),
	subs_email_address_footer = $("#femailaddress").val(),
	arr = [];
	$subsformId_footer.submit(function(event){
			$("#sub_alert_msg").hide("slow");
			subs_email_address_footer = $("#femailaddress").val();
			subs_username_footer = $("#fusername").val();
			$.ajax({
				url: subs_url_footer,
				type  : "POST",
				data : { email : subs_email_address_footer,name : subs_username_footer },
				success: function(result){
          console.log("event:success", result);
					arr = $.parseJSON(result);
					if(arr.length == 0){
						$("#subscription_popup").removeClass('subscription-popup').addClass('subscription-popup2');
						$(".overlay").fadeIn( "slow" );
						$("#subscription_form_footer")[0].reset();
					}else{
						$("#sub_alert_msg").show("slow");
						$("#sub_alert_msg p").html(arr['error']);
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					// console.log("event:failure", jqXHR);
          // console.log("event:failure", textStatus);
          // console.log("event:failure", errorThrown);
					$("#sub_alert_msg").show("slow");
					$("#sub_alert_msg p").html("Hmph! Something doesn't seem right. Please contact The Hampstead Kitchen for further assistance.");
				}
			});
		   return false;
		});
	var subs_url_top = $("#subscription_form").attr('action'),
	$subsformId_top = $("#subscription_form"),
	subs_email_address_top = $("#emailaddress").val(),
	arr = [];
	$subsformId_top.submit(function(event){
			$("#sub_alert_msg1").hide("slow");
			subs_email_address_top = $("#emailaddress").val();
			subs_username_top = $("#username").val();
			$.ajax({
				url: subs_url_top,
				type  : "POST",
				data : {email : subs_email_address_top, name : subs_username_top },
				success: function(result){
					arr = $.parseJSON(result);
					if(arr.length == 0){
						$(".overlay").fadeIn( "slow" );
						$("#subscription_form")[0].reset();
					}else{
						$("#sub_alert_msg1").show("slow");
						$("#sub_alert_msg1 p").html(arr['error']);
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					$("#sub_alert_msg1").show("slow");
					$("#sub_alert_msg1 p").html("Hmph! Something doesn't seem right. Please contact The Hampstead Kitchen for further assistance.");
				}
			});
		   return false;
		});

		var $subscribeformId_sp = $("#subscription_form_subpage"),
		subscribe_url_sp = $subscribeformId_sp.attr('action'),
		arr = [];
		$subscribeformId_sp.submit(function(event){
				$("#sub_alert_msg_sp").hide("slow");
				subscribe_email_address_sp = $("#emailaddress_sp").val();
				subscribe_username_sp = $("#username_sp").val();
				$.ajax({
					url: subscribe_url_sp,
					type  : "POST",
					data : {email : subscribe_email_address_sp, name : subscribe_username_sp },
					success: function(result){
						arr = $.parseJSON(result);
						if(arr.length == 0){
							$("#subpage-form").slideUp( "slow" );
							$("#subpage-cm").slideDown( "slow" );
							$subscribeformId_sp[0].reset();
						}else{
							$("#sub_alert_msg_sp").show("slow");
							$("#sub_alert_msg_sp p").html(arr['error']);
						}
					},
					error: function(jqXHR, textStatus, errorThrown) {
						$("#sub_alert_msg_sp").show("slow");
						$("#sub_alert_msg_sp p").html("Hmph! Something doesn't seem right. Please contact The Hampstead Kitchen for further assistance.");
					}
				});
			   return false;
			});

	$(".post-detail img").addClass('img-responsive');

	// Read more - who we are
	 $( ".p_more a" ).click(function( event ) {
		event.preventDefault();
		if($(this).hasClass('more'))
		{
			$(this).removeClass('more less').addClass('less');
			$(".hidden_text").show('slow');
			$(".hidden_image").show('slow');
			$(this).html('Read Less');
		}else
		{
			$(this).removeClass('more less').addClass('more');
			$(".hidden_text").hide('slow');
			$(".hidden_image").hide('slow');
			$(this).html('Read more');
		}
	});
	//latest 10 posts
	 var cposts = $('.blog-page div.testim-item-row').length;
	 $( "#blog_nav_posts" ).click(function( event ) {
		event.preventDefault();
		if($("#blog_nav_posts").hasClass('moreposts'))
		{
			$(".blog-mpContainer li").removeClass('blog_post_hidden');
			$("#blog_nav_posts a").html('Less posts');
			$("#blog_nav_posts").removeClass('moreposts').addClass('lessposts');
			$(".blog-page .scrib-pomegranate-3").hide();
			$(".white-paper").hide();
		}else
		{
			$(".blog-mpContainer li:nth-child(n+6)").addClass('blog_post_hidden');
			$("#blog_nav_posts a").html('More posts');
			$("#blog_nav_posts").addClass('moreposts').removeClass('lessposts');
			$("#blog_nav_posts").removeClass('blog_post_hidden');
			if(cposts <= 4){

			}else if(cposts == 5){
				$(".blog-page .scrib-pomegranate-3").addClass('hidden-sm hidden-xs').show();
			}else{
				$(".blog-page .scrib-pomegranate-3").addClass('hidden-sm hidden-xs').show();
				$(".white-paper").addClass('hidden-sm hidden-xs').show();
			}
		}
	});

	var deferreds = [];
    	var imgs = $('#carousel-banner', this).find('img');
    	imgs.each(function(){
        var self = $(this);
        var datasrc = self.attr('src');
        if (datasrc) {
            var d = $.Deferred();
            self.one('load', d.resolve)
                .attr("src", datasrc);
            deferreds.push(d.promise());
        }
    });

    $.when.apply($, deferreds).done(function(){
        $('#carousel-banner').carousel({
					interval: 4000,
		  			wrap:	true
				});
    });
	
	
	// New Slider Script
	
	$("#homeMainSlideshow > div:gt(0)").hide();

		setInterval(function() { 
		  $('#homeMainSlideshow > .slide-item:first')
			.fadeOut(2000)
			.next()
			.fadeIn(2000)
			.end()
			.appendTo('#homeMainSlideshow');
		},  5000);
	
	// New Slider Script End


    if(cposts <= 4){
	$(".blog-page .scrib-pomegranate-3").hide();
	$(".white-paper").hide();
	}else if(cposts == 5){
		$(".white-paper").hide();
		}else{}
	$('.nav .dropdown-menu li .letsTalk_btn').on('click touchstart',function(event){
		$.wait(500).then(function() {
  			$(".nav .dropdown").removeClass('open');
		}, function(reason) {
			});
	});
  	$(".post-social").on('click', 'a.share-popup', function() {
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;
        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
        var left = ((width / 2) - (300)) + dualScreenLeft;
        var top = ((height / 2) - (125)) + dualScreenTop;
        window.open($(this).attr('href'), 'cnvSocialShare','left:' + left + ',top:' + top + ',height=250,width=600,toolbar=no,status=no');
        return false;
      });


});
</script>

<?php Loader::element('footer_required'); ?>
</body>
</html>
