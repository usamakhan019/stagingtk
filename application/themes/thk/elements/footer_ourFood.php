<?php defined('C5_EXECUTE') or die("Access Denied.");?>



<!-- Footer -->
<?php if ($c->isEditMode()) { ?>
<footer class="footer mar-t-200">
<?php }else{ ?>
<footer id="subscribe" class="footer">
<?php } ?>

    <div class="container">
        <div class="row">
            <div class="col-sm-7 col-md-4 col-xs-12 contact-us">
				<?php
					$area_contact = new GlobalArea('contact_us');
					$area_contact->display($c);
				?>
            </div>
            <div class="col-sm-5 col-md-2 col-xs-12">
					<?php
						$area_footer_c1_nav = new GlobalArea('nav_col_1');
						$area_footer_c1_nav->display($c);
					?>
            </div>
            <div class="col-sm-6 col-md-3 col-xs-12">
				<?php
					$area_social = new GlobalArea('social_network');
					$area_social->display($c);
				?>
            </div>
            <div class="col-sm-6 col-md-3 col-xs-12">
				<div class="subscribe_form">
				<?php
					$area_subscribe = new GlobalArea('subscribe');
					$area_subscribe->display($c);
				?>
				</div>

            </div>
        </div>
        <div class="row mar-t-20 mar-b-20">
            <div class="text-center">
				<?php
					$area_trademark = new GlobalArea('trademark');
					$area_trademark->display($c);
				?>
            </div>
        </div>
    </div><!-- / .container -->
</footer>
<?php if ($c->isEditMode()) { ?>
<div class="overlay" style="display : block;">
<?php }else{ ?>
<div class="overlay" style="display : none;">
<?php } ?>

    <div id="subscription_popup" class="subscription-popup">
		<?php
					$area_trademark = new GlobalArea('subscription_popup');
					$area_trademark->display($c);
		?>

        <div class="text-center">
            <button class="btn btn-default-sm text-center" id="popupClose">Close</button>
        </div>
    </div>
</div>

<?php $this->inc('elements/footer_bottom.php');?>


