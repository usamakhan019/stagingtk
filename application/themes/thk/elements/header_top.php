<?php defined('C5_EXECUTE') or die("Access Denied."); ?>
<!DOCTYPE html>
<html lang="<?php echo Localization::activeLanguage()?>">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="https://plus.google.com/b/100168854849356600344/+Thehampsteadkitchenprivatechefandcatering" rel="publisher" />
    <link href="<?php echo $view->getThemePath()?>/css/reset.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $view->getThemePath()?>/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo $view->getThemePath()?>/css/styles.css" rel="stylesheet" type="text/css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <?php Loader::element('header_required')?>
    <script>
        if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
            var msViewportStyle = document.createElement('style')
            msViewportStyle.appendChild(
                document.createTextNode(
                    '@-ms-viewport{width:auto!important}'
                )
            )
            document.querySelector('head').appendChild(msViewportStyle)
        }
     	function feedBackFormRequest(){
    		var url = window.location.href,
    		pos = url.search("#letstalk-section");
    		if (pos != -1){
    			$(".letsTalk-info").hide('slow');
    			$(".letsTalk-form").show('slow');
    		}
    	}
     	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      	ga('create', 'UA-45525697-1', 'auto');
      	ga('send', 'pageview');

      <!-- Hotjar Tracking Code for http://www.thehampsteadkitchen.com -->
      (function(h,o,t,j,a,r){
          h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
          h._hjSettings={hjid:155294,hjsv:5};
          a=o.getElementsByTagName('head')[0];
          r=o.createElement('script');r.async=1;
          r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
          a.appendChild(r);
      })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');


</script>
<style type="text/css">

</style>
</head>
<body onload="feedBackFormRequest()">
<!-- Google Code for Online Enquiry Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 948657140;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "kIrLCMjg7WQQ9LetxAM";
var google_remarketing_only = false;
/* ]]> */
</script>
<div style="display:none !important;">
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
</div>
<noscript>
<img height="1" width="1" style="border-style:none;" alt=""
src="//www.googleadservices.com/pagead/conversion/948657140/?label=kIrLCMjg7WQQ9LetxAM&amp;guid=ON&amp;script=0"/>
</noscript>
<div class="<?php echo $c->getPageWrapperClass()?>">
