<?php defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header_top.php');
$page = Page::getCurrentPage();
$nh = Loader::helper('navigation');
$URL = $nh->getCollectionURL($page);
?>
<header>
	<nav class="navbar navbar-inverse navbar-fixed-top custom_z_index" role="navigation">
	
			<div class="badge"><?php $area_header_nav = new GlobalArea('header_ badge');$area_header_nav->display($c); ?></div> 
			<!-- <a href="javascript:void(0)" id="hideBadge" class="hideBadge">
				<i class="glyphicon glyphicon-remove"></i>
			</a>      -->
		</section>
		<div class="navbar-inner">
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-xs-12 nav-logo">
						<?php
							$area_header_nav = new GlobalArea('header_ navigation_logo');
							$area_header_nav->display($c);
						?>
					</div>
					<div class="col-md-9 col-xs-12 nav-col hidden-sm hidden-xs">
						
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<div class="col-md-9 col-xs-9">
								<?php
									$area_header_nav = new GlobalArea('header_ navigation');
									$area_header_nav->display($c);
							?>
							</div>
							<div class="col-md-3 col-xs-3">
							<?php
									$area_header_nav = new GlobalArea('header_ navigation_letsChat');
									$area_header_nav->display($c);
							?>
							</div>
						</div>
					</div>
				</div> <!-- row -->
			</div> <!-- container -->
		</div> <!-- Nav inner -->
	</nav>
	<nav class="navbar navbar-inverse navbar-bottom navbar-fixed-bottom just-for-responsive visible-xs visible-sm hidden-md">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12 nav-col">
                <div class="navbar-header mar-t5 pull-left">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-bottom">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button> 
                </div>
                <div class="collapse navbar-collapse pull-left" id="navbar-bottom">
                    <?php
						$area_header_nav = new GlobalArea('header_ navigation');
			        	$area_header_nav->display($c);
			    	?>

                </div>
                <ul class="nav navbar-nav navbar-right navbar-letstalk pull-right">
	<li class="dropdown"><a aria-expanded="false" role="button" data-toggle="dropdown" class="dropdown-toggle nav-letstalk-zi" href="#">Let's Chat <img src="/application/files/8514/3548/4794/icon_letsTalk.png" alt="Saima Khan - Founder &amp; Private Chef &amp; Caterer for The Hampstead Kitchen" height="54" width="54"></a>
	<ul role="menu" class="dropdown-menu">
		<li><h3>LET'S CHAT...</h3></li>
		<li class="letschat"><img  src="/application/files/9214/3548/4697/icon_lt-phone.png" alt="Contact Details of private chef &amp; caterer at The Hampstead Kitchen" height="34" width="48"><span>07540 764 359</span></li> 
		<li class="text-h4"><a target="_blank" href="mailto:saima@thehampsteadkitchen.com"><img src="/application/files/5014/3548/4696/icon_lt-email.png" alt="Contact Details of private chef &amp; caterer atThe Hampstead Kitchen" height="34" width="48">saima@thehampsteadkitchen.com</a>
		<!--<li-->
		<a href="http://thehampsteadkitchen.com/#letstalk-section" class="btn-noteus letsTalk_btn"><img src="/application/files/8314/3548/4697/icon_lt-note.png" alt="Contact Details of private chef &amp; caterer atThe Hampstead Kitchen" class="pull-left" height="34" width="48"><span class="btn-note"></span></a></li>
	</ul></li>
	<li class="dropdown open"></li>
</ul>
                <!-- /.navbar-collapse -->
            </div>    
        </div><!-- Logo and Ad section End -->
    </div><!-- /Header Section Container -->
</nav>
</header>

<!-- <script>
$(document).ready(function(){
  $(window).scroll(function() {
    if ($(this).scrollTop()>0)
    {
      $('.badge').fadeOut();
      if ($('header').width() >= 768){
        $("#homeMainSlideshow").css({"margin": "68px 0 0"});
      }else{
        $("#homeMainSlideshow").css({"margin": "55px 0 0"});
      }

     
    }
 });
});
</script> -->