<?php defined('C5_EXECUTE') or die("Access Denied.");
$view->inc('elements/header.php');
?>

<!-- Page Content -->
<div id="instafeed" style="display : none;"></div>
<section class="home-banner">
	<div class="">
    <div class="banner-heading-wrapper">
      <div class="main-heading-style text-center">
        <div class="hidden-sm hidden-xs">
            <?php
              $header_area = new Area('header_content_area');
              $header_area->display($c);
            ?>
        </div>
      </div>
    </div><!-- /.row -->
	</div><!-- /.container -->
</section><!-- section banner -->

<div id="carousel-banner" class="carousel slide carousel-fade hidden" data-ride="carousel" data-wrap="false" >
      <!-- Carousel items -->
     <div class="carousel-inner">
      <!-- <div class="active item">
                    <img alt="Sharing Platter Mezze  Buffet, Feast & banquet by The Hampstead Kitchen" src="<?php echo $view->getThemePath()?>/images/home-slider/landingSlider_05.jpg">
            </div> -->
            <div class="active item">
                    <img alt="Sharing Platter Mezze  Buffet, Feast & banquet by The Hampstead Kitchen" src="<?php echo $view->getThemePath()?>/images/home-slider/landingSlider_04.jpg">
            </div>
            <div class="item">
                    <img alt="Sharing Platter Mezze  Buffet, Feast & banquet by The Hampstead Kitchen" src="<?php echo $view->getThemePath()?>/images/home-slider/landingSlider_01.png">
            </div>
            <div class="item">
                    <img alt="Sharing Platter Mezze  Buffet, Feast & banquet by The Hampstead Kitchen" src="<?php echo $view->getThemePath()?>/images/home-slider/landingSlider_03.png">
            </div>
            <!-- <div class="item">
                    <img alt="Sharing Platter Mezze  Buffet, Feast & banquet by The Hampstead Kitchen" src="<?php echo $view->getThemePath()?>/images/home-slider/landingSlider_02.jpg">
            </div> -->
        </div>
</div><!-- Old Slider Hidden -->

<div id="homeMainSlideshow">
   <!-- <div class="slide-item">
     <img alt="Sharing Platter Mezze  Buffet, Feast & banquet by The Hampstead Kitchen" src="<?php echo $view->getThemePath()?>/images/home-slider/landingSlider_05.jpg">
   </div> -->
   <div class="slide-item">
     <img alt="Sharing Platter Mezze  Buffet, Feast & banquet by The Hampstead Kitchen" src="<?php echo $view->getThemePath()?>/images/home-slider/landingSlider_04.jpg">
   </div>
   <div class="slide-item">
     <img alt="Sharing Platter Mezze  Buffet, Feast & banquet by The Hampstead Kitchen" src="<?php echo $view->getThemePath()?>/images/home-slider/landingSlider_01.png">
   </div>
   <div class="slide-item">
     <img alt="Sharing Platter Mezze  Buffet, Feast & banquet by The Hampstead Kitchen" src="<?php echo $view->getThemePath()?>/images/home-slider/landingSlider_03.png">
   </div>
   <!-- <div class="slide-item">
     <img alt="Sharing Platter Mezze  Buffet, Feast & banquet by The Hampstead Kitchen" src="<?php echo $view->getThemePath()?>/images/home-slider/landingSlider_02.jpg">
   </div> -->
</div>

<!-- New Slider -->

<section class="about-section">
	<div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
              <div class="hidden-md hidden-lg">
                <?php
                  $thk_title_area_ipad_phone = new Area('thk_title_area_ipad_phone');
                  $thk_title_area_ipad_phone->display($c);
                ?>
              </div>
              <div class="hidden-sm hidden-xs">
                <?php
                  $thk_title_area_desktop = new Area('thk_title_area_desktop');
                  $thk_title_area_desktop->display($c);
                ?>
              </div>
           
              <?php
                $about_area = new Area('about_content_area');
                $about_area->display($c);
              ?>
			
            </div><!-- Col 12 -->
            <div class="absec-divider"></div>
        </div><!-- /.row -->
	</div><!-- /.container -->
</section><!-- about section -->

<section class="videos-section">
	<div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
			<?php
				$video_h_area = new Area('video_header_area');
				$video_h_area->display($c);
			?>
			 <div class="video-cantainer">
				<?php
					$video_c_area = new Area('video_content_area');
					$video_c_area->display($c);
				?>
			</div>

			<?php
				$video_f_area = new Area('video_footer_area');
				$video_f_area->display($c);
			?>

            </div><!-- Col 12 -->

            <div class="fly scrib-rose3 mobile-hide"></div>
            <div class="fly scrib-parsley1 mobile-hide"></div>
            <div class="vdsec-divider"></div>
        </div><!-- /.row -->
	</div><!-- /.container -->
</section><!-- videos section -->

<section class="testimonial-section">
	<div class="just-for-xs">
        <div class="clearfix">
            <div class="col-md-12 text-center">
				<?php
					$imSlider_area = new Area('slider_area');
					$imSlider_area->display($c);
				?>
            </div><!-- Col 12 -->
            <!-- <div class="fly scrib-anjeer mobile-hide"></div> -->
            <div class="ltsec-divider"></div>
        </div><!-- /.row -->
	</div><!-- /.container -->
</section><!-- Testimonial section -->  


<section class="letstalk-section" id="letstalk-section">
	<div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
              <div class="three-rose-petal hidden-sm hidden-lg hidden-md"></div>
				<?php
					$letstalk_h_area = new Area('letstalk_h_area');
					$letstalk_h_area->display($c);
				?>
				<a id="letsTalkSec"></a>
          <div class="wboard-cantainer">
					<div class="letsTalk-info">
						<?php
						$letstalk_c_area = new Area('letstalk_content_area');
						$letstalk_c_area->display($c);
						?>
					</div>

					<div class="letsTalk-form" style="display: none;">
						<?php
							$feedback_area = new Area('feedback_area');
							$feedback_area->display($c);
						?>
					</div>
					<div class="success_Message">
					<?php
							$successMessage_area = new Area('success_message');
							$successMessage_area->display($c);
						?>
					</div>


				</div>
                <div class="red"></div>
                <div class="instaFeed-divider"></div>
            </div><!-- Col 12 -->
        </div><!-- /.row -->
	</div><!-- /.container -->
</section><!-- letstalk section -->

<section class="instagram-feed-home-page">
    <div class="container">
      <div class="row">
        <div class="col-sm-4 text-center mobile-hide hidden-sm"></div>
        <div class="col-sm-12 col-md-4 text-center">
          <?php
            $area_video = new Area("Instagram feed header");
            $area_video->display($c);
          ?>
        </div>
        <div class="divider-25"></div>
      </div>
      <div class="col-md-12">
        <div id="carousel-instagramfeed" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner" role="listbox" id="carousel_container"></div>
          <a class="left carousel-control" href="#carousel-instagramfeed" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#carousel-instagramfeed" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
    </div>
</section>

<div class="overlay" id="notification-modal" style="display : block;">
    <div id="notification" class="info-notification">
      <?php
            $area_trademark = new GlobalArea('notification');
            $area_trademark->display($c);
      ?>
      <div class="text-center">
        <button class="hover-line" id="notificationClose">Close</button>
      </div>
      <div class="fly scrib-pomegranate zindex11 hidden-sm hidden-xs"></div>
      <div class="fly scrib-basil zindex11 hidden-sm hidden-xs"></div>
    </div>
    
</div>

<?php $view->inc('elements/footer.php'); ?>
