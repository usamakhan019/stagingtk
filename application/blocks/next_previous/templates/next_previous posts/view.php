<?php defined('C5_EXECUTE') or die("Access Denied.");
$nh = Loader::helper('navigation');
$pre_src = '';
$next_src = '';
$previousLinkURL = is_object($previousCollection) ? $nh->getLinkToCollection($previousCollection) : '';
$parentLinkURL = is_object($parentCollection) ? $nh->getLinkToCollection($parentCollection) : '';
$nextLinkURL = is_object($nextCollection) ? $nh->getLinkToCollection($nextCollection) : '';
$previousLinkText = is_object($previousCollection) ? $previousCollection->getCollectionName() : '';
$nextLinkText = is_object($nextCollection) ? $nextCollection->getCollectionName() : '';
$thumbnail = false;

if($previousLinkURL != '')
{
	$pre_thumbnail = $previousCollection->getAttribute('thumbnail');
	if(is_object($pre_thumbnail)){
		$pre_src = $pre_thumbnail->getRelativePath();	
	}
	
}

if($nextLinkURL != '')
{
	$next_thumbnail = $nextCollection->getAttribute('thumbnail');
	if(is_object($next_thumbnail)){
		$next_src = $next_thumbnail->getRelativePath();	
	}
	
}
?>

<?php if ($previousLinkURL || $nextLinkURL || $parentLinkText): ?>

<div class="blog-mpContainer">
<ul>
    <?php if ($previousLabel && $previousLinkURL != ''): ?>
    <div class="ccm-block-next-previous-header">
       <h2 class="mar-t0 text_center"><?php echo $nextLabel?></h2>
    </div>
    <?php endif; ?>

    <?php if ($previousLinkText): ?>
	<li><a href="<?php echo $previousLinkURL  ?>"><img alt="<?php echo $previousLinkText ?>" src="<?php echo $pre_src; ?>" /><?php echo $previousLinkText  ?></a></li>      
	<?php endif; ?>

    <?php if ($nextLabel && $nextLinkURL != ''): ?>
         <h2 class="mar-t0 text_center"><?php echo $previousLabel?></h2>
    <?php endif; ?>

    <?php if ($nextLinkText): ?>
        <li><a href="<?php echo $nextLinkURL  ?>"><img alt="<?php echo $nextLinkText ?>" src="<?php echo $next_src; ?>" /><?php echo $nextLinkText  ?></a></li> 
    <?php endif; ?>

    <?php if ($parentLinkText): ?>
	<p class="ccm-block-next-previous-parent-link">
		<?php echo $parentLinkURL ? '<a href="' . $parentLinkURL . '">' . $parentLinkText . '</a>' : '' ?>
 	</p>
	<?php endif; ?>
</ul>
</div>

<?php endif; ?>
