<?php defined('C5_EXECUTE') or die("Access Denied.");

$nh = Loader::helper('navigation');
$th = Loader::helper('text');
$imgHelper = Loader::helper('image'); 
$c = Page::getCurrentPage();
$count = 0;
$note='';
$src='';
$more = 0;

if ($c->isEditMode()) { ?>
    <div class="ccm-edit-mode-disabled-item" style="width: <?php echo $width; ?>; height: <?php echo $height; ?>">
        <div style="padding: 20px 0px 20px 0px"><?php echo t('Image Slider disabled in edit mode.')?></div>
    </div>
<?php  } else { ?>

<?php
$list = new \Concrete\Core\Page\PageList();
$list->filterByPageTypeHandle('blog_entry');
$list->sortByPublicDateDescending();
$pages = $list->getResults();
//$pages = array_reverse($pages);
//$content = $pages[0]->getBlocks('Content');
if(sizeof($pages) > 0)
{ ?>
	<div id="carousel-blogposts" class="carousel slide" data-ride="carousel">                
                  <div class="carousel-inner" role="listbox"> 
				<?php	foreach ($pages as $blogpost){
							$src="";
							$title = $th->entities($blogpost->getCollectionName());
							$url = $nh->getLinkToCollection($blogpost);
							$target = ($blogpost->getCollectionPointerExternalLink() != '' && $blogpost->openCollectionPointerExternalLinkInNewWindow()) ? '_blank' : $blogpost->getAttribute('nav_target');
							$target = empty($target) ? '_self' : $target;
							
							$datePublic = date('M d, Y', strtotime($blogpost->getCollectionDatePublic()));
							$description = $blogpost->getCollectionDescription();
							
							
							$thumbnail = $blogpost->getAttribute('thumbnail');
							if(is_object($thumbnail)){
								$src = $thumbnail->getRelativePath();}
							if($count == 0){ ?>
								<div class="item active row">
						<?php }else{ ?>
							<div class="item row">
					<?php	} ?>
				
                        
								<div class="col-md-10 col-md-offset-2 hdng-col">
									<h2><?php echo($title); ?></h2>
								</div>
								<div class="col-md-3 col-md-offset-1">
									<div class="testim-img-container testim-item1 clearfix">
										<div class="testim-img"></div>
										<img width="308" height="320" alt="<?php echo $title ?>" src="<?php echo $src; ?>" class="testim-img-only">
									</div>
								</div>
								<div class="col-md-7 testim-text">
									<p><?php echo ($description); ?></p>
									<a href="<?php echo $url;?>" class="btn btn-default-md">Read more</a>
								</div>
							</div>
                        <?php $count++;} ?>
                  </div>
                
                  <!-- Controls -->
                  <a class="left carousel-control" href="#carousel-blogposts" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#carousel-blogposts" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
			
	
<?php }else {?> 
	<div class="ccm-image-slider-placeholder">
            <p><?php echo t('No Blog Posts.'); ?></p>
    </div>
	<?php }?>

<?php } ?>
