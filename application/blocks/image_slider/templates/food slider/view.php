<?php defined('C5_EXECUTE') or die("Access Denied.");
$nh = Loader::helper('navigation');
$c = Page::getCurrentPage();
$count = 0;
$note='';
$more = 0;

if ($c->isEditMode()) { ?>
    <div class="ccm-edit-mode-disabled-item" style="width: <?php echo $width; ?>; height: <?php echo $height; ?>">
        <div style="padding: 60px 0px 20px 0px"><?php echo t('Image Slider disabled in edit mode.')?></div>
    </div>
<?php  } else { ?>


	<?php if(count($rows) > 0) { ?>
		<div id="ourfood-carousel" class="carousel slide" data-ride="carousel">    
			<div class="carousel-inner" role="listbox">
				<?php foreach($rows as $row) { 
					$f = File::getByID($row['fID']);
					$path = $f->getRelativePath(); 
					if($count == 0){ ?>
						
						<div class="item active">
						
					<?php $count++;}else {?>
					
						<div class="item">
						
					<?php } ?>
					
						<img src="<?php echo $path ?>" height="500" alt="<?php echo $row['title'] ?>">
					</div>
				<?php } ?>
				
			
			
				<!-- Controls -->
				<a class="left carousel-control" href="#ourfood-carousel" role="button" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#ourfood-carousel" role="button" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
				
				<section class="food-container">
					<div class="container">
						<div class="row">
							<div class="col-md-9 col-sm-12 col-xs-12 text-center">
								<span class="cat-name"><?php echo ($c->getCollectionName()) ?></span>
								<ol class="carousel-indicators  hidden-xs">
									<?php 
										for($i=0; $i<count($rows); $i++){
											if($i == 0){ ?>
											<li data-target="#ourfood-carousel" data-slide-to="<?php echo ($i);?>" class="active"></li>
											<?php }else{ ?>
												<li data-target="#ourfood-carousel" data-slide-to="<?php echo ($i);?>"></li>
										<?php }
									 }?>    
								</ol>
								<span class="cat-ddown">
									<div class="dropup inline">
										  <a class="dropdown-toggle dropdown-ourfood" id="dropdownOurfood" data-toggle="dropdown" aria-expanded="true">
											more<span class="caret"></span>
										  </a>
										   <ul class="dropdown-menu dropdown-menu-ourfood" role="menu" aria-labelledby="dropdownOurfood">
										  
										  <?php $source_page = Page::getByPath('/our-food');
											$food_categories = $source_page->getCollectionChildrenArray();
											foreach($food_categories as $pg_ID){ 
											$c_pg = Page::getByID($pg_ID);
											$url = $nh->getLinkToCollection($c_pg);									
											?>
											<li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo $url ?>"><?php echo $c_pg->getCollectionName() ?></a></li>
										<?php	}
										  ?>
										  </ul>
									</div>
								</span>
							</div><!-- End Col-9 -->
							<div class="col-md-3 hidden-sm hidden-xs">
								<a href="http://instagram.com/hampsteadkitchn" target="_blank" class="fl-in pull-right" title="See us on Instagram"><span class="ft-icons icon-in"></span></a>
								<a href="https://plus.google.com/b/100168854849356600344/+Thehampsteadkitchenprivatechefandcatering" target="_blank" class="fl-gp pull-right" title="Find us on google +"><span class="ft-icons icon-gp"></span></a>
								<a href="https://twitter.com/hampsteadkitchn" target="_blank" class="fl-tw pull-right" title="Follow us on Twitter"><span class="ft-icons icon-tw"></span></a>
								<a href="https://www.facebook.com/hampstead.kitchen" target="_blank" class="fl-fb pull-right" title="Like us on Facebook"><span class="ft-icons icon-fb"></span></a>
							</div>
						</div> <!-- End Row -->
					</div><!-- End Container -->
				</section> <!-- End section -->
				
			</div><!-- inner slider -->
		</div><!-- End slider -->
	<?php }else {?> 
	<div class="ccm-image-slider-placeholder">
            <p><?php echo t('No Slides Entered.'); ?></p>
    </div>
	<?php }?>

<?php } ?>
