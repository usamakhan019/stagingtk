<?php defined('C5_EXECUTE') or die("Access Denied.");

$c = Page::getCurrentPage();
$count = 0;
$note='';
$more = 0;

$source_page = Page::getByPath('/our-clients');
$blocks = $source_page->getBlocks('Branding Testimonials'); ?>

	<div id="carousel-testimonial" class="carousel slide" data-ride="carousel">                
        <!-- Wrapper for slides -->
		<div class="carousel-inner" role="listbox">
			<?php foreach($blocks as $block){
				$more = 0;
				if($count != 0){ ?><div class="item row"><?php } else{ ?> <div class="item active row"> <?php } 
				$count++;
				$f = File::getByID($block->getInstance()->fID);
				$path = $f->getRelativePath(); 
				$note = $block->getInstance()->paragraph;
				if(strlen($note) > 300){
					$note = substr($note ,0 , strpos($note , ' ' , 300));
					$more = 1;
				}
			?> 
			
			<div class="col-md-3 col-md-offset-2">
				<div class="testim-img-container testim-item clearfix">
					<div class="testim-img"></div>
					<img width="308" height="320" alt="Catering by the Private Chef from The Hampstead Kitchen" src="<?php echo $path ?>" class="testim-img-only">
				</div>
			</div>
                
			<div class="col-md-5 testim-text">
				<div class="bq-inner text_left">
					<?php echo $note; if($more){?><span class="dot-dot">..</span><?php } ?>
					<footer>
						<cite title="Source Title">
							<?php echo ($block->getInstance()->name); ?>, <?php if ($block->getInstance()->position) :	echo ($block->getInstance()->position); endif;?>
							<?php if ($block->getInstance()->company){
								if($block->getInstance()->companyURL){ ?>
									<br>
									<a href="<?php echo ($block->getInstance()->companyURL) ?>" target="_blank"><?php echo ($block->getInstance()->company); ?></a>
								<?php } else {
									echo ($block->getInstance()->company);
								}		
							} ?>
						</cite>
					</footer>
				</div>	
			</div>
        	</div>
			<?php } ?>
		</div>
			
		<!-- Controls -->
        <a class="left carousel-control" href="#carousel-testimonial" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#carousel-testimonial" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>