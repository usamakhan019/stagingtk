<?php
defined('C5_EXECUTE') or die("Access Denied.");
$th = Loader::helper('text');
$imgHelper = Loader::helper('image'); 
$c = Page::getCurrentPage();
$src='';
$count = 0;
$dh = Core::make('helper/date'); /* @var $dh \Concrete\Core\Localization\Service\Date */
?>

<?php if ( $c->isEditMode() && $controller->isBlockEmpty()) { ?>
    <div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Page List Block.')?></div>
<?php } else { ?>

<div class="row food">
	<?php foreach ($pages as $page):
		$src="";
		$title = $th->entities($page->getCollectionName());
		$url = $nh->getLinkToCollection($page);
		$target = ($page->getCollectionPointerExternalLink() != '' && $page->openCollectionPointerExternalLinkInNewWindow()) ? '_blank' : $page->getAttribute('nav_target');
		$target = empty($target) ? '_self' : $target;
		//$description = $page->getCollectionDescription();
		//$description = $controller->truncateSummaries ? $th->wordSafeShortText($description, $controller->truncateChars) : $description;
		//$description = $th->entities($description);
        $thumbnail = false;
        if ($displayThumbnail) {
            $thumbnail = $page->getAttribute('thumbnail');
			if(is_object($thumbnail)){
			$src = $thumbnail->getRelativePath();	}		
        } 
		if($count == 4){ ?>
			<div class="col-md-4 col-sm-6 hidden-sm">
				<div class="of-frameEmpty">
				<img alt="Arabesque Garden Party" src="/application/themes/thk/images/of_plate.png" class="food-plate">
				</div>
			</div>
		<?php }
		?>
		<div class="col-md-4 col-sm-6">
		<a href="<?php echo ($url); ?>">
        	<div class="of-frame">
            	<div class="h4 food_list_title"><?php echo t($title); ?></div>
				<div class="h4 food_onhover_action">Click to dive in</div>
            </div>
            <img alt="<?php echo t($title); ?>" src="<?php echo t($src); ?>" class="ofood-img">
		</a>
        </div>
		
		
		
	<?php $count++; 
		  endforeach; ?>
	<div class="fly scrib-parsley1 mobile-hide"></div>
	<div class="fly scrib-rose3 mobile-hide"></div>
	<div class="fly scrib-olives mobile-hide hidden-sm"></div>
	<div class="fly doscrib4 mobile-hide"></div>
	<div class="fly doscrib8 mobile-hide"></div>
</div>

<?php if ($showPagination): ?>
    <?php echo $pagination;?>
<?php endif;?>

<?php } ?>