<?php
defined('C5_EXECUTE') or die("Access Denied.");
$th = Loader::helper('text');
$imgHelper = Loader::helper('image'); 
$c = Page::getCurrentPage();
$src='';
$dh = Core::make('helper/date'); 
?>

<?php if ( $c->isEditMode() && $controller->isBlockEmpty()) { ?>
    <div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Page List Block.')?></div>
<?php } else { ?>
	<div class="blog-mpContainer">
		<ul>
		<?php 
			foreach ($pages as $lfpage):
				$src="";
				$lftitle = $th->entities($lfpage->getCollectionName());
				$lfurl = $nh->getLinkToCollection($lfpage);
				$lftarget = ($lfpage->getCollectionPointerExternalLink() != '' && $lfpage->openCollectionPointerExternalLinkInNewWindow()) ? '_blank' : $lfpage->getAttribute('nav_target');
				$lftarget = empty($lftarget) ? '_self' : $lftarget;
				$thumbnail = false;
				if ($displayThumbnail) {	
					$lfthumbnail = $lfpage->getAttribute('thumbnail');
					if(is_object($lfthumbnail)){
						$src = $lfthumbnail->getRelativePath();	
					}
				}
				?>
				<li><a href="<?php echo $lfurl; ?>"><img  src="<?php echo $src; ?>" alt="<?php echo $lftitle; ?>" /><?php echo $lftitle; ?></a></li> 
            <?php endforeach; ?>
		</ul>
    </div>
		


<?php } ?>
