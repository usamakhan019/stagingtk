<?php
defined('C5_EXECUTE') or die("Access Denied.");
$th = Loader::helper('text');
$c = Page::getCurrentPage();
$count=0;
$src = "";
$dh = Core::make('helper/date'); /* @var $dh \Concrete\Core\Localization\Service\Date */
?>

<?php if ( $c->isEditMode() && $controller->isBlockEmpty()) { ?>
    <div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Page List Block.')?></div>
<?php } else { ?>



    <?php foreach ($pages as $page):
		$src="";
		$count++;
		$title = $th->entities($page->getCollectionName());
		$url = $nh->getLinkToCollection($page);
		$target = ($page->getCollectionPointerExternalLink() != '' && $page->openCollectionPointerExternalLinkInNewWindow()) ? '_blank' : $page->getAttribute('nav_target');
		$target = empty($target) ? '_self' : $target;
		$description = $page->getCollectionDescription();
        $description = $controller->truncateSummaries ? $th->wordSafeShortText($description, $controller->truncateChars) : $description;
        $description = $th->entities($description);	
        $thumbnail = false;
       
		
        $date = $dh->formatDateTime($page->getCollectionDatePublic(), true);

		$thumbnail = $page->getAttribute('thumbnail');
			if(is_object($thumbnail)){
				$src = $thumbnail->getRelativePath();
				}
			
		
		?>
		<div class="col-md-6 col-sm-6">
			<div class="blog-post post text-center">
				<h2><a href="<?php echo ($url) ?>"><?php echo ($title) ?></a></h2>
				<a href="<?php echo ($url) ?>">
					<div class="post-img-container">
						<div class="post-img-frame fly"></div>
						<img src="<?php echo ($src); ?>" width="205" alt="<?php echo ($title) ?>" >
					</div>
				</a>
				<p><?php echo ($description) ?></p>
			</div>
		</div>

	<?php endforeach; 
	
	if (count($pages) == 0): ?>
        <div class="ccm-block-page-list-no-pages"><?php echo h($noResultsMessage)?></div>
    <?php endif;

	}?>


