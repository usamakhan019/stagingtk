<?php
// More BLogs Posts
defined('C5_EXECUTE') or die("Access Denied.");
$th = Loader::helper('text');
$imgHelper = Loader::helper('image'); 
$c = Page::getCurrentPage();
$src='';
$count=0;
$dh = Core::make('helper/date');
?>

<?php if ( $c->isEditMode() && $controller->isBlockEmpty()) { ?>
    <div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Page List Block.')?></div>
<?php } else { ?>

	<div class="blog-mpContainer">
		<ul>
		
		<?php 
			Loader::model('page_list');
			$pagination = Loader::helper('pagination');
			$list = new PageList();
			$list->filterByPageTypeHandle('blog_entry');
			$list->setItemsPerPage(10);
			$list->sortByPublicDateDescending();
			$pagination = $list->getPagination();
			$pagination->setMaxPerPage(10)->setCurrentPage(1);
			$pages = $pagination->getCurrentPageResults();
			foreach ($pages as $latestpost):
				$src="";
				$count++;
				$title = $th->entities($latestpost->getCollectionName());
				$url = $nh->getLinkToCollection($latestpost);
				$target = ($latestpost->getCollectionPointerExternalLink() != '' && $latestpost->openCollectionPointerExternalLinkInNewWindow()) ? '_blank' : $latestpost->getAttribute('nav_target');
				$target = empty($target) ? '_self' : $target;
				$thumbnail = false;
				if ($displayThumbnail) {	
					$thumbnail = $latestpost->getAttribute('thumbnail');
					if(is_object($thumbnail)){
						$src = $thumbnail->getRelativePath();	
					}
				}
				?>
				<li class="<?php if($count > 5): echo "blog_post_hidden"; endif; ?>"><a href="<?php echo $url; ?>"><img alt="<?php echo $title; ?>" src="<?php echo $src; ?>" /><?php echo $title; ?></a></li>		
            <?php endforeach; 
			if($count  > 5) {?>
			<li id="blog_nav_posts" class="moreposts"><a href="#">More Posts</a></li>
			<?php } ?>
		</ul>
    </div>
<?php if ($showPagination): ?>
    <?php echo $pagination;?>
<?php endif; 
		


 } ?>
