<?php
defined('C5_EXECUTE') or die("Access Denied.");
$th = Loader::helper('text');
$c = Page::getCurrentPage();
$count=0;
$src = "";
$dh = Core::make('helper/date'); /* @var $dh \Concrete\Core\Localization\Service\Date */
?>

<?php if ( $c->isEditMode() && $controller->isBlockEmpty()) { ?>
    <div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Page List Block.')?></div>
<?php } else { ?>



    <?php foreach ($pages as $page):
		$count++;
		$title = $th->entities($page->getCollectionName());
		$url = $nh->getLinkToCollection($page);
		$target = ($page->getCollectionPointerExternalLink() != '' && $page->openCollectionPointerExternalLinkInNewWindow()) ? '_blank' : $page->getAttribute('nav_target');
		$target = empty($target) ? '_self' : $target;
		$description = $page->getCollectionDescription();
        $description = $controller->truncateSummaries ? $th->wordSafeShortText($description, $controller->truncateChars) : $description;
        $description = $th->entities($description);	
        $thumbnail = false;
        if ($displayThumbnail) {
            $thumbnail = $page->getAttribute('thumbnail');
				if(is_object($thumbnail)){
					$src = $thumbnail->getRelativePath();
					}
			}
        $date = $dh->formatDateTime($page->getCollectionDatePublic(), true);  
		
		?>
		
		<h2 class="text-center"><a href="<?php echo ($url) ?>">Latest Blog</a></h2>
		<a href="<?php echo ($url) ?>">
			<div class="post-img-container">
            			<div class="post-img-frame fly"></div>
				<img src="<?php echo ($src); ?>" width="205" alt="Latest Blog" >
        		</div>
		</a>
		<p><?php echo ($description) ?></p>

	<?php endforeach; 
	if($count == 0)
	{ ?>
			<a href="#"><h2 class="text-center">Latest Blog</h2></a>
				
	<?php }

 } ?>