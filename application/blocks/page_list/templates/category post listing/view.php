<?php
defined('C5_EXECUTE') or die("Access Denied.");
$th = Loader::helper('text');
$c = Page::getCurrentPage();
$src = "";
$count = 1;
$dh = Core::make('helper/date'); /* @var $dh \Concrete\Core\Localization\Service\Date */
?>

<?php if ( $c->isEditMode() && $controller->isBlockEmpty()) { ?>
    <div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Page List Block.')?></div>
<?php } else { ?>



    <?php foreach ($pages as $page):
		$src="";
		if($count > 7)
		{
			$count = 1;
		}
		$title = $th->entities($page->getCollectionName());
		$url = $nh->getLinkToCollection($page);
		$target = ($page->getCollectionPointerExternalLink() != '' && $page->openCollectionPointerExternalLinkInNewWindow()) ? '_blank' : $page->getAttribute('nav_target');
		$target = empty($target) ? '_self' : $target;
		$description = $page->getCollectionDescription();
        $description = $controller->truncateSummaries ? $th->wordSafeShortText($description, $controller->truncateChars) : $description;
        $description = $th->entities($description);	
        $thumbnail = false;
       
		
        $date = $dh->formatDateTime($page->getCollectionDatePublic(), true);

		$thumbnail = $page->getAttribute('thumbnail');
			if(is_object($thumbnail)){
				$src = $thumbnail->getRelativePath();
				}
		
		?>
		<div class="row testim-item-row"> 
			<div class="col-md-4">
				<a href="<?php echo ($url) ?>">
				<div class="testim-item<?php echo $count ?> clearfix">
					<div class="testim-img"></div>
						<img width="308" height="320" alt="<?php echo ($title) ?>" src="<?php echo ($src) ?>" class="testim-img-only">
				</div>
				</a>
			</div>		
			<div class="col-md-8 testim-img-container">
				<div class="bq-inner">
					<h3><a href="<?php echo ($url) ?>"><?php echo ($title); ?></a></h3>
					<p><?php echo ($description); ?></p>
				</div>
			</div>
		</div>

	<?php $count++; endforeach; 
	
	if (count($pages) == 0): ?>
        <div class="ccm-block-page-list-no-pages"><?php echo h($noResultsMessage)?></div>
    <?php endif; ?>



<?php if ($showPagination): ?>
    <?php echo $pagination;?>
<?php endif; 

	}?>