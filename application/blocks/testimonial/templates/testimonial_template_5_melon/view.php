<?php  defined('C5_EXECUTE') or die("Access Denied.");
?>
<div class="row testim-item-row">
	<div class="col-md-3">
		<div class="testim-item5 clearfix">

			<div class="testim-img"></div>
			<img width="308" height="320" alt="Catering by the Private Chef from The Hampstead Kitchen" src="<?php echo($image->src);?>" class="testim-img-only">
		</div>
	</div>
	<div class="col-md-9 testim-img-container">
		<div class="bq-inner">
			<?php echo t($paragraph);?>
		</div>
		<footer>
			<p><?php echo ($name); ?>, <?php if ($position) :	echo ($position); endif;?></p>
			<?php if ($company){
				if($companyURL){ ?>
					<a href="<?php echo ($companyURL) ?>" target="_blank"><?php echo ($company); ?></a>
				<?php } else {
					echo ($company);
				}		
			} ?>
		</footer>
</div>
</div><!-- /.row -->