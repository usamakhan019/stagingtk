<?php

/**
 * -----------------------------------------------------------------------------
 * Generated 2020-11-27T10:11:04+00:00
 *
 * @item      misc.do_page_reindex_check
 * @group     concrete
 * @namespace null
 * -----------------------------------------------------------------------------
 */
return array(
    'site' => 'thehampsteadkitchen',
    'version_installed' => '5.7.4RC2',
    'misc' => array(
        'access_entity_updated' => 1431049551,
        'latest_version' => '5.7.4.2',
        'favicon_fid' => '79',
        'do_page_reindex_check' => false
    ),
    'cache' => array(
        'blocks' => true,
        'assets' => false,
        'theme_css' => true,
        'overrides' => true,
        'pages' => 'blocks',
        'full_page_lifetime' => 'default',
        'full_page_lifetime_value' => null
    ),
    'theme' => array(
        'compress_preprocessor_output' => true
    ),
    'mail' => array(
        'method' => 'php_mail',
        'methods' => array(
            'smtp' => array(
                'server' => 'smtp.gmail.com',
                'username' => 'saima@thehampsteadkitchen.com',
                'password' => 'iman6509',
                'port' => '587',
                'encryption' => 'TLS'
            )
        )
    ),
    'seo' => array(
        'canonical_url' => '',
        'canonical_ssl_url' => '',
        'redirect_to_canonical_url' => 0,
        'url_rewriting' => 1
    )
);
